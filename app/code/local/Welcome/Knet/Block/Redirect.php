<?php
require_once 'Welcome/Knet/Block/e24PaymentPipe.inc.php';
class Welcome_Knet_Block_Redirect extends Mage_Core_Block_Template
{
    /**
     * Initializing the parameters
     * Performing the payment action
     */
     protected function _toHtml()
    {
        //Get the base directory and url
        $base_dir = Mage::getBaseDir();
        $base_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        //Get the security key
        $security_key = Mage::getModel('Welcome_Knet_Model_Cc')->getConfigData('security_key');
        //Generate the random number for trackid
        $trackid_rand = rand(11111111,99999999);
        //Gett the order amount details.
        $price  = number_format($this->_getOrder()->getBaseGrandTotal(),2,'.','');
        $order_id = $this->_getOrder()->getIncrementId();
        $storeCode = Mage::app()->getStore()->getCode();
        if($storeCode == "arabic") {
            $lang = "ARA";
        } else {
            $lang = "ENG";
        }
        $websiteId = Mage::app()->getWebsite()->getId();
        if($websiteId != 1) {
            $cur_code = Mage::app()->getStore()->getCurrentCurrencyCode();
            $to_code = 'KWD';
            $price = number_format(Mage::helper('directory')->currencyConvert($this->_getOrder()->getBaseGrandTotal(), $cur_code, $to_code), 2, '.', '');
        }
        $Pipe = new e24PaymentPipe;
        $Pipe->setAction(1);
        $Pipe->setCurrency(414);
        $Pipe->setLanguage($lang); //change it to "ARA" for arabic language
        $Pipe->setResponseURL($base_url."response.php");
        $Pipe->setErrorURL($base_url."error.php");
        $Pipe->setAmt($price);
        //$Pipe->setResourcePath("/Applications/MAMP/htdocs/php-toolkit/resource/");
        $Pipe->setResourcePath($base_dir."/knetresource/");
        $Pipe->setAlias($security_key);
        $Pipe->setTrackId($trackid_rand);
        $Pipe->setUdf1($order_id);
        $Pipe->setUdf2(Mage::app()->getStore()->getStoreId());
        $Pipe->setUdf3("");
        $Pipe->setUdf4("");
        $Pipe->setUdf5("");

        //get results
        if($Pipe->performPaymentInitialization()!=$Pipe->SUCCESS){

            Mage::log('failed',null,'knet_elli.log');
            $data = array('order_id'=>$order_id,'amount'=>$price, 'paymentid'=>'', 'result'=>'','postdate'=>'','tranid'=>'','auth'=>'','ref'=>'', 'trackid' =>$trackid_rand, 'udf1' =>$order_id, 'udf2' =>Mage::app()->getStore()->getStoreId(), 'udf3' =>'', 'udf4' =>'', 'udf5' =>'');
            $model = Mage::getModel('knet/knet')->setData($data);
            $model->save();

            $this->_debug($Pipe->getErrorMsg());
            Mage::getSingleton('checkout/session')->addError("There is an issue with your payment process. Kindly contact the site administrator.");
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('checkout/cart'))->sendResponse();
            exit;
        } else {

            $payID = $Pipe->getPaymentId();
            $payURL = $Pipe->getPaymentPage();

            $data = array('order_id'=>$order_id,'amount'=>$price, 'paymentid'=>$payID, 'result'=>'','postdate'=>'','tranid'=>'','auth'=>'','ref'=>'', 'trackid' =>$trackid_rand, 'udf1' =>$order_id, 'udf2' =>Mage::app()->getStore()->getStoreId(), 'udf3' =>'', 'udf4' =>'', 'udf5' =>'');
            $model = Mage::getModel('knet/knet')->setData($data);
            $model->save();

            $order = $this->_getOrder()->setPaymentid($payID);
            $order->save();

            // echo $Pipe->getDebugMsg();
            $location = $payURL."?PaymentID=".$payID;
            Mage::log($data,null,'knet_elli.log');
            $html = '<html><body>';
            $html.= $this->__('You will be redirected to KNET in a few seconds.');
            $html.= '<form id="knetpayment_checkout" method="POST" action='.$location.'>';
            $html.= '</form>';
            $html.= '<script type="text/javascript">document.getElementById("knetpayment_checkout").submit();</script>';
            $html.= '</body></html>';
            return $html;
        }
    }

     /**
     * Return checkout session instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Return order instance
     *
     * @return Mage_Sales_Model_Order|null
     */
    protected function _getOrder()
    {
        if ($this->getOrder()) {
            return $this->getOrder();
        } elseif ($orderIncrementId = $this->_getCheckout()->getLastRealOrderId()) {
            return Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
        } else {
            return null;
        }
    }

     /**
     * Log debug data to file
     *
     * @param mixed $debugData
     */
    protected function _debug($debugData)
    {
        if (Mage::getStoreConfigFlag('payment/knet_cc/debug')) {
            Mage::log($debugData, null, 'payment_knet_cc.log', true);
        }
    }

}