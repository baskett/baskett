<?php
class Welcome_Knet_Block_Form extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('knet/form.phtml');
    }

    protected function _getConfig()
    {
        return Mage::getSingleton('knet/config');
    }
}