<?php
class Welcome_Knet_Block_Success extends Mage_Core_Block_Template
{
    public function getOrderInfo()
    {
        $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
        $order_id = $order->getId();
        $isVisible = !in_array($order->getState(),Mage::getSingleton('sales/order_config')->getInvisibleOnFrontStates());
        $order->addData(array(
                'is_order_visible' => $isVisible,
                'view_order_id' => $this->getUrl('sales/order/view/', array('order_id' => $order_id)),
                'view_order_url' => $this->getUrl('sales/order/view/', array('order_id' => $order_id)),
                'print_url' => $this->getUrl('sales/order/print', array('order_id'=> $order_id)),
                'can_print_order' => $isVisible,
                'can_view_order'  => Mage::getSingleton('customer/session')->isLoggedIn() && $isVisible,
                'order_id'  => $order->getIncrementId(),
            ));
        return $order;
    }

    public function getOrderDetails()
    {
        $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
        $result = Mage::getModel('knet/knet')->load($orderId, 'order_id');
        return  $result;
    }
}
