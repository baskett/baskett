<?php
class Welcome_Mobile_Model_Api2_Coupon_Add_Rest_Guest_V1 extends Welcome_Mobile_Model_Api2_Customer_Addcart
{
   protected function _create()
    {
        $post = $this->getRequest()->getBodyParams();
        if ($post) {
            $customer_id = $post['customer_id'];
            //$storeId = ($post['lang'] == 'ar') ? 3 : 1;
            if($post['lang']){
                $store = Mage::getModel('core/store')->load($post['lang']);
                $storeId = $store->getStoreId();
                $websiteId = $store->getWebsiteId();
                $lang = (strchr($post['lang'], 'ar') == false) ? 1 : 3 ;
            }
            else{
                $websiteId = 1;
                $storeId = 19;
                $lang = 1;
            }
            
            $quoteId = $post['quote_id'];
            if($quoteId){
                $couponCode = $post['code'];
                
                try {
                    $codeLength = strlen($couponCode);
                    $isCodeLengthValid = $codeLength && $codeLength <= Mage_Checkout_Helper_Cart::COUPON_CODE_MAX_LENGTH;

                    $store = Mage::getSingleton('core/store')->load($storeId);
                    $quote = Mage::getModel('sales/quote')->setStore($store)->load($quoteId);

                    $quote = $quote->setCouponCode($isCodeLengthValid ? $couponCode : '')->collectTotals()->save();

                    if ($codeLength) {
                        if ($isCodeLengthValid && $couponCode == $quote->getCouponCode()) {
                            $message = $this->__('Coupon code "%s" was applied.', Mage::helper('core')->escapeHtml($couponCode));
                        } else {
                            $message = $this->__('Coupon code "%s" is not valid.', Mage::helper('core')->escapeHtml($couponCode));
                        }
                    } else {
                        $message = $this->__('Coupon code was canceled.');
                    }

                } catch (Mage_Core_Exception $e) {
                    $message = $e->getMessage();
                } catch (Exception $e) {
                    $message = $this->__('Cannot apply the coupon code.');
                    Mage::logException($e);
                }
            }else{
                $message = $this->__('Invalid quote Id.');
            }
        }else{
            $message = $this->__('Invalid Coupon info');
        }
        return array('message' => $message);
    }
}
