<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Welcome_Mobile_Model_Api2_Guestregister_Rest_Guest_V1 extends Welcome_Mobile_Model_Api2_Guestregister
{
    protected function getCustomerByEmail($customer, $email){
        $customer->loadByEmail($email);
        return $customer;
    }
    public function _create()
    {
        $requestData = $this->getRequest()->getBodyParams();
		$guestLoginLogFile = 'guestlogin_'.date('Y-m-d').'.log';
		Mage::log($requestData,null,$guestLoginLogFile);
        if ($requestData) {
            try {
                if($requestData['lang']){
                    $store = Mage::getModel('core/store')->load($requestData['lang']);
                    $storeId = $store->getStoreId();
                    $websiteId = $store->getWebsiteId();
                    $lang = (strpos($requestData['lang'], 'ar') === false) ? 1 : 3 ;
                }
                else{
                    $storeId = 1;
                    $websiteId = 1;
                    $lang = 1;
                }
                $email = $requestData['email'];
                $error = false;
                //validate email address
                if (!Zend_Validate::is(trim($email), 'EmailAddress')) {
                    $error = true;
                    $message = ($lang == 3) ? Mage::helper('customer')->__('عنوان البريد الإلكتروني غير صالح .') : Mage::helper('customer')->__('Invalid Email Address.');
                }
                if ($error) {
                    throw new Exception();
                }
                $customer = Mage::getModel('customer/customer')
                            ->setWebsiteId($websiteId);
                try {
                    $customer = $this->getCustomerByEmail($customer, $email);
                    //encrypt customer id
                    $encrypt = Mage::getSingleton('core/encryption');
                    if($customer->getId()){
                        $message = ($lang == 3) ? Mage::helper('customer')->__('هناك بالفعل حساب مع هذا عنوان البريد الإلكتروني') : Mage::helper('customer')->__('There is already an account with this email address');
                        throw new Exception($message);
                    }
                    else{
                        $customer->setEmail($email);
                        $newPassword = $customer->generatePassword();
                        $customer->setFirstname('Guest');
                        $customer->setLastname('User');
                        $customer->setPassword($newPassword);
                        $customer->setWebsiteId($websiteId);
                        $customer->save();
                        $customer->sendPasswordReminderEmail();
                    }
                    $subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($customer->getEmail());
                    $status = $subscriber->isSubscribed();
                    $customer_id = $encrypt->encrypt($customer->getId());
                    $customer_id = bin2hex($customer_id);
                    $message = ($lang == 3) ? Mage::helper('customer')->__('تسجيل العملاء بنجاح') : Mage::helper('customer')->__('Customer successfully registered');    
                    $gender = '';
                    if($customer->getGender() == 1){
                        $gender = 'Male';
                    }
                    if($customer->getGender() == 2){
                        $gender = 'Female';
                    }
                    $info = array('login_status' => 'Success', 
                            'message' => $message, 'customer_id' => $customer_id , 'first_name' => $customer->getFirstname(),
                            'last_name' => $customer->getLastname(),
                            'email' => $customer->getEmail(),
                            'gender' => $gender,
                            'dob' => $customer->getDob(),
                            'newsletter_subscription' => $status);
                    $address = $customer->getPrimaryBillingAddress();
                    $address_detail = array();
                    if(!empty($address)){
                        $address_detail = array(
                        'mobile'=> $address->getTelephone(),
                        'landline' => $address->getAddrLandline());
                    }else{
                        $address_detail = array(
                        'mobile'=> '',
                        'landline' => ''
                        );
                    }   
                    $json = array_merge($info,$address_detail);
					Mage::log($json,null,$guestLoginLogFile);
                    echo json_encode($json);
                    exit();

                }catch(Exception $e){
                    $error = true;
                    $message = $e->getMessage();
                }
                if ($error) {
                    throw new Exception();
                }
                
            } catch (Exception $e) {
                $message = Mage::helper('customer')->__($message);
				Mage::log($message,null,$guestLoginLogFile);
                $json = array('customer_id' => '', 'login_status' => 'error', 'message' => $message);
                echo json_encode($json);
                exit();            
            }
        }
    }
}