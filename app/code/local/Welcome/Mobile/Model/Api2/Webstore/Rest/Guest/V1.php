<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Welcome_Mobile_Model_Api2_Webstore_Rest_Guest_V1 extends Welcome_Mobile_Model_Api2_Webstore
{   
	protected function _retrieveCollection()
    {
        $response = array();
            try {
                $combination = array();
                $i = 0;
                foreach (Mage::app()->getWebsites() as $website) {
                	$combination[$i]['website_id'] = $website['website_id'];
                	$combination[$i]['code'] = $website['code'];
                    $combination[$i]['name'] = $website['name'];
                    $combination[$i]['is_default'] = $website['is_default'];
                    foreach ($website->getGroups() as $group) {
                        $stores = $group->getStores();
							//$combination[$i]['stores'][$group['name']] = $group['name'];
                        foreach ($stores as $store) {
                            $combination[$i]['stores'][$group['name']][$store['code']]['store_id'] = $store['store_id'];
                            $combination[$i]['stores'][$group['name']][$store['code']]['name'] = $store['name'];
                            $combination[$i]['stores'][$group['name']][$store['code']]['code'] = $store['code'];
                            $combination[$i]['stores'][$group['name']][$store['code']]['is_active'] = $store['is_active'];
                        }
                    }
                    $i++;
	            }
                $response['combination'] = array('websites'=>$combination);
            }catch (Exception $e) {
            } 
        return $response;
    }
}
