<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Welcome_Mobile_Model_Api2_Products_Rest_Guest_V1 extends Welcome_Mobile_Model_Api2_Products
{
     /**
     * Current loaded product
     *
     * @var Mage_Catalog_Model_Product
     */
    protected $_product;

    /**
     * Retrieve product data
     *
     * @return array
     */
    protected function _retrieve()
    {
        die("dead");
        $product = $this->_getProduct();
        $productsArray = array();
        $_product = $this->_prepareProductForResponse($product);
        //return $product->getData();
        $productsArray[] = $_product;
        return $productsArray;
    }

    /**
     * Retrieve list of products
     *
     * @return array
     */
    protected function _retrieveCollection()
    {
		//error_reporting(-1);
		//ini_set('display_errors', 'On');
        $apiUrl = Mage::helper('core/url')->getCurrentUrl();
        Mage::log('Start - Category Api - '.$apiUrl,null,'api_record.log');
        /** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $categoryId = $this->getRequest()->getParam('category_id');
        $store = $this->_getStore();
        $storeId = $store->getId();
        $websiteId = $store->getWebsiteId();
        if($page = $this->getRequest()->getParam('page')){
            $pageId = (isset($page) && $page != 0) ? $page : 1;
        }
        if($count = $this->getRequest()->getParam('count')){
            $count = (isset($count) && $count != 0) ? $count : 20;
        }
        $key = "api_category_products_".$websiteId."_".$storeId."_".$pageId."_".$count;
        if ($categoryId) {
            $category = $this->_getCategoryById($categoryId);
            if (!$category->getId()) {
                $this->_critical('Category not found.', Mage_Api2_Model_Server::HTTP_BAD_REQUEST);
            }
            $key .= "_".$categoryId;
            $filterAttr = $this->getRequest()->getParam('filter');
            $filterValue = $this->getRequest()->getParam('value');
            if ($filterAttr) {
                $key .= "_".$filterValue;
            }
        }
        try {
            //$mobileHelper = Mage::helper('mobile');
            //$redisClient = $mobileHelper->getRedisClient();
            //echo $redisClient->load('ellison');
            //$productsArray = unserialize($redisClient->load($key));
            if($productsArray){
                Mage::log('Hit - '.$key.' - '.$apiUrl,null,'api_record.log');
                Mage::log('End - Category Api - '.$apiUrl,null,'api_record.log');
                return $productsArray;
            }
        } catch (Exception $e) {
            Mage::log('Error in Cache',null,'api_record.log');
        }
        Mage::log('Miss - '.$key.' - '.$apiUrl,null,'api_record.log');
        $collection = Mage::getResourceModel('catalog/product_collection');

        $collection->setStoreId($store->getId());
        $entityOnlyAttributes = $this->getEntityOnlyAttributes($this->getUserType(),
            Mage_Api2_Model_Resource::OPERATION_ATTRIBUTE_READ);
        $availableAttributes = array_keys($this->getAvailableAttributes($this->getUserType(),
            Mage_Api2_Model_Resource::OPERATION_ATTRIBUTE_READ));
        // available attributes not contain image attribute, but it needed for get image_url
        $availableAttributes[] = 'image';
        $availableAttributes = array_diff($availableAttributes, array('description', 'meta_title', 'meta_description','meta_keyword', 'tier_price', 'sku'));
        $collection->addStoreFilter($store->getId())
            ->addPriceData($this->_getCustomerGroupId(), $store->getWebsiteId())
            ->addAttributeToSelect(array_diff($availableAttributes, $entityOnlyAttributes))
            ->addAttributeToFilter('visibility', array(
                'neq' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE))
            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED));
        $this->_applyCategoryFilter($collection);
        //$this->_applyCollectionModifiers($collection);
        $products = $collection->load();
        $productsArr = array();
        /** @var Mage_Catalog_Model_Product $product */
        foreach ($products as $product) {
            $this->_setProduct($product);
            $pro = $this->_prepareProductForCollection($product);
            $productsArr[] = $pro;
        }
        //return $products->toArray();
        
        
        if($categoryId = $this->getRequest()->getParam('category_id')){
            $catpro['products'][] = $productsArr;
            $catpro['page_info'][] = array('total_pages'=>$collection->getLastPageNumber(), 'total_items'=>$collection->getSize());
            //$catpro['page_info'][] = array('total_items'=>$collection->count());
            //$redisClient->save(serialize($catpro),$key, array('api_cache', 'category_products'), 60*60);
            Mage::log('End - Category Api - '.$apiUrl,null,'api_record.log');
            //$catpro['page_info'][1] = array('page_number'=>(!$this->getRequest()->getParam('page')) ? $this->getRequest()->getParam('page') : 1);
            return $catpro;
        }else{
            //$redisClient->save(serialize($productsArr),$key, array('api_cache', 'category_products'), 60*60);
            Mage::log('End - Category Api - '.$apiUrl,null,'api_record.log');
            return $productsArr;
        }   
    }

    /**
     * Apply filter by category id
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     */
    protected function _applyCategoryFilter(Mage_Catalog_Model_Resource_Product_Collection $collection)
    {
        $categoryId = $this->getRequest()->getParam('category_id');
        //$celebrityCategory = Mage::helper('celebrity')->getCelebrityCategoryIds();
        if ($categoryId) {
            $category = $this->_getCategoryById($categoryId);
            if (!$category->getId()) {
                $this->_critical('Category not found.', Mage_Api2_Model_Server::HTTP_BAD_REQUEST);
            }
            $collection->addCategoryFilter($category);
            // add sorting based on ad date for celebrity products
            /*if(in_array($categoryId, $celebrityCategory)){
                $collection->getSelect()->joinLeft("celebrity_sorting","e.entity_id = celebrity_sorting.product_id && celebrity_sorting.celebrity_id = $categoryId", array('celebrity_datetime' => 'celebrity_sorting.ad_date', 'ad_number' => 'celebrity_sorting.ad_number'))->group('e.entity_id');
                $collection->getSelect()->order('celebrity_datetime desc');
            }*/
            $this->_applyLayeredFilter($collection);
        }
    }
    protected function _applyLayeredFilter(Mage_Catalog_Model_Resource_Product_Collection $collection)
    {
        $filterAttr = $this->getRequest()->getParam('filter');
        $filterValue = $this->getRequest()->getParam('value');
        //if ($filterAttr  && $filterAttr=='manufacturer') {
            //$collection->addAttributeToFilter($filterAttr, $filterValue);
        //}
        $filterVal = explode(',', $filterValue);
        if ($filterAttr  && $filterAttr=='manufacturer') {
            $collection->addAttributeToFilter($filterAttr, array('in'=>$filterVal));
        }
        $page = $this->getRequest()->getParam('page');
        $count = $this->getRequest()->getParam('count');
        
        $paging = (isset($page) && $page != 0) ? $page : 1;
        $count = (isset($count) && $count != 0) ? $count : 20;
        $collection->setPageSize($count)->setCurPage($paging);
    }
    

    /**
     * Add special fields to product get response
     *
     * @param Mage_Catalog_Model_Product $product
     */
    protected function _prepareProductForCollection(Mage_Catalog_Model_Product $product)
    {
        /** @var $productHelper Mage_Catalog_Helper_Product */
        $productHelper = Mage::helper('catalog/product');
        $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
		//$rating = Mage::getModel('review/review_summary')->setStoreId($this->_getStore()->getId())->load($product->getId());
		//$reviewModel = Mage::getModel('review/review');
        $product = Mage::getModel('catalog/product')->setStoreId($this->_getStore()->getId())->load($product->getId());
        $categoryId = $this->getRequest()->getParam('category_id');
        //$celebrityCategory = Mage::helper('celebrity')->getCelebrityCategoryIds();
        $max = unserialize($product->getMax());
        $maxorder = unserialize($product->getMaxorder());
        $productData = array();
        // calculate prices
        $finalPrice = $product->getFinalPrice();
        //get product price and name based on celebrity
        /*if($this->getRequest()->getParam('category_id')){
            $category = Mage::getModel('catalog/category')->load(95);
            $celebrityCat = explode(',', $category->getChildren());
            $categoryId = $this->getRequest()->getParam('category_id');
            //$categoryIds = Mage::helper('celebrity')->getCelebrityCategoryIds();
            //$splname = ((in_array($categoryId, $categoryIds) || in_array($categoryId, $celebrityCat)) && $product->getSpecialName()) ? $product->getSpecialName() : $product->getName();
            $splname = $product->getName();
            $data = unserialize($product->getCelebritySpecialPrice());
            $adnumber = unserialize($product->getCelebrityAdNumber());
            $finalPrice = isset($data[$categoryId]) ? (float)$data[$categoryId] : $product->getFinalPrice();
            $label = isset($adnumber[$categoryId]) ? (int)$adnumber[$categoryId] : null;
        }
        else{
            $label = null;
            $finalPrice = $product->getFinalPrice();
            $splname = $product->getName();
        }*/
        // $product->setWebsiteId(1);
        // customer group is required in product for correct prices calculation
        $product->setCustomerGroupId($this->_getCustomerGroupId());
        	
        $productData['entity_id'] = $product->getEntityId();
        $productData['short_description'] = $product->getShortDescription();
        $productData['name'] = $product->getName();
        $productData['regular_price'] = (string) $product->getPrice();
        $productData['final_price'] = (string) $finalPrice;
        //$productData['exclusive'] = (int) $product->getExclusive();
        //$productData['exclusive_celebrity'] = $product->getExclusiveOnCelebrity();
		//$productData['rating'] = $rating->getRatingSummary();
		//$productData['total_reviews_count'] = $reviewModel->getTotalReviews($product->getId(), true,
                //$this->_getStore()->getId());
        //if($label!=null){
           //$productData['product_label'] = $label; 
        //}
        $productData['is_saleable'] = $stock->getIsInStock();
        /*if(in_array($categoryId, $celebrityCategory) && $stock->getIsInStock()==1){
            if(is_array($max) && is_array($maxorder)){
                if (isset($max[$categoryId])) {
                    if($maxorder[$categoryId] >= $max[$categoryId]){
                       $productData['is_saleable'] = 0;
                    }
                }               
            }
        }*/
        //$productData['image_url'] = (string) Mage::helper('catalog/image')->init($product, 'image');
        $productData['image_url'] = $product->getImageUrl();
        /** Fetch Custom Option **/
        $options = Mage::getModel('catalog/product_option')->getProductOptionCollection($product);
        $_custom_options = array();
        foreach ($options as $option) {
            $_custom_options[$option->getOptionId()] = array('name' => $option->getDefaultTitle(),
                                                            'type' => $option->getType());
            $values = Mage::getSingleton('catalog/product_option_value')->getValuesCollection($option);
            $i = 1;
            foreach ($option->getValues() as  $value) {
                $_custom_options[$option->getOptionId()]['value'][] = array('title' => $value->getTitle(),
                                                                            'price' => $value->getPrice(),'price_type' => $value->getPriceType(),);
           
                $i++;
            }
        }
        /** End of Fetch Custom Option **/
        $productData['has_custom_options'] = count($options) > 0;
        $productData['custom_options'] = (!empty($_custom_options))?$_custom_options:'';
        return $productData;
    }
    /**
     * Load product by its SKU or ID provided in request
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function _getProduct()
    {
        if (is_null($this->_product)) {
            $productId = $this->getRequest()->getParam('id');
            /** @var $productHelper Mage_Catalog_Helper_Product */
            $productHelper = Mage::helper('catalog/product');
            $product = $productHelper->getProduct($productId, $this->_getStore()->getId());
            if (!($product->getId())) {
                $this->_critical(self::RESOURCE_NOT_FOUND);
            }
            // check if product belongs to website current
            if ($this->_getStore()->getId()) {
                $isValidWebsite = in_array($this->_getStore()->getWebsiteId(), $product->getWebsiteIds());
                if (!$isValidWebsite) {
                    $this->_critical(self::RESOURCE_NOT_FOUND);
                }
            }
            // Check display settings for customers & guests
            if ($this->getApiUser()->getType() != Mage_Api2_Model_Auth_User_Admin::USER_TYPE) {
                // check if product assigned to any website and can be shown
                if ((!Mage::app()->isSingleStoreMode() && !count($product->getWebsiteIds()))
                    || !$productHelper->canShow($product)
                ) {
                    $this->_critical(self::RESOURCE_NOT_FOUND);
                }
            }
            $this->_product = $product;
        }
        return $this->_product;
    }

    /**
     * Set product
     *
     * @param Mage_Catalog_Model_Product $product
     */
    protected function _setProduct(Mage_Catalog_Model_Product $product)
    {
        $this->_product = $product;
    }

    /**
     * Load category by id
     *
     * @param int $categoryId
     * @return Mage_Catalog_Model_Category
     */
    protected function _getCategoryById($categoryId)
    {
        return Mage::getModel('catalog/category')->load($categoryId);
    }  
    protected function _getCustomerGroupId()
    {
        return null;
    }
    protected function _getStore()
    {
        $storeCode = $this->getRequest()->getParam('lang');
        try {
            if($storeCode){
                $store = Mage::getModel('core/store')->load($storeCode);
                $storeId = $store->getStoreId();
                $websiteId = $store->getWebsiteId();
            }
            $store = Mage::app()->getStore($storeId);
        } catch (Mage_Core_Model_Store_Exception $e) {
            // store does not exist
            $this->_critical('Requested store is invalid', Mage_Api2_Model_Server::HTTP_BAD_REQUEST);
        }
        return $store;
    }
}
