<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Welcome_Mobile_Model_Api2_Home_Homeproductslist_Rest_Guest_V1 extends Welcome_Mobile_Model_Api2_Home_Homeproductslist
{
	/**
	 * Retrieve list of Homeproducts List.
	 *
	 * @return array
	 */
	protected function _retrieveCollection()
	{
		$homeproducts = array();
		$post = $this->getRequest()->getParams();
        if($post['lang']){
        	$store = Mage::getModel('core/store')->load($post['lang']);
        	$storeId = $store->getStoreId();
			
        	$websiteId = $store->getWebsiteId();
        }
        else{
        	$storeId = 1;
        }
		
        $key = "api_home_home_productslist_".$storeId;
        try {
            $mobileHelper = Mage::helper('mobile');
            //$redisClient = $mobileHelper->getRedisClient();
            //$redisClient->remove($key);
            //$homeProducts = unserialize($redisClient->load($key));
            
            //if($homeProducts){
                //Mage::log('Hit - Homeproducts List Api',null,'api_record.log');
                //return $homeProducts;
            //}
        } catch (Exception $e) {
            Mage::log('Error in Cache',null,'api_record.log');
        }
        Mage::log('Miss - Homeproducts List Api',null,'api_record.log');
        Mage::app()->setCurrentStore($storeId);
		$newproducts = $this->newproducts($storeId);
		$banner = $this->bannerSlider($storeId);
		$featuredproducts = $this->featuredproducts($storeId);
		$adds=$this->getAdds($storeId);
		$homeproducts['banner'][] = $banner;
		$homeproducts['ads'][]=$adds;
	    $homeproducts['newproducts'][] = $newproducts;
		$homeproducts['featuredproducts'][] = $featuredproducts;

       	//$redisClient->save(serialize($homeproducts),$key, array('api_cache', 'home_product'), 60*60*24);
	    return $homeproducts;
	}
	public function getadds($storeId){
        $ad_1_image = Mage::getModel('core/variable')->setStoreId($storeId)->loadByCode('ad_1_image')->getValue('text');
        $ad_1_url = Mage::getModel('core/variable')->loadByCode('ad_1_url')->getValue('text');
        $ad_2_image = Mage::getModel('core/variable')->loadByCode('ad_2_image')->getValue('text');
        $ad_2_url = Mage::getModel('core/variable')->loadByCode('ad_2_url')->getValue('text');
        $adArray[0]['image'] = $ad_1_image;
        $adArray[0]['url'] = $ad_1_url;
        $adArray[1]['image'] = $ad_2_image;
        $adArray[1]['url'] = $ad_2_url;
        
        return $adArray;
        
		$block = Mage::getModel('cms/block')->setStoreId($storeId)->load('main_slide_right');
		//print_r($block);die;
		$media_url =  Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
		$title=$block->getTitle();
		$content=$block->getContent();
		$isActive = $block->getIsActive();
		$text = html2text($content);
		echo $text;die;
		if($isActive){
			$blockImage=$this->linkExtractor($content);
			print_r($blockImage);die;
		}else{
			
		}
		//preg_match_all('/<img[^>]+>/i',$content, $contentImage); 
		//print_r($contentImage);die;
		
	}

	public function linkExtractor($html){
	 $linkArray = array();
	 if(preg_match_all('/<img\s+.*?src=[\"\']?([^\"\' >]*)[\"\']?[^>]*>/i',$html,$matches,PREG_SET_ORDER)){
		 print_r($matches);die;
	  foreach($matches as $match){
	   array_push($linkArray,array($match[1],$match[2]));
	  }
	 }
	 return $linkArray;
	}
	//get newarrivals products
	public function newproducts($storeId){
		$newproducts = array();
		//$filter = 'new';
		$collection = Mage::getSingleton('catalog/product')->getCollection()->setStoreId($storeId);
		$collection->addAttributeToSelect('*');
		$collection->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
		$visibility = array(
				Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
				Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG
			);
		$collection->addAttributeToFilter('visibility', $visibility);
		$collection->setOrder('entity_id','DESC');

		
		Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
		$collection->setPage(1, 10);
        
		foreach($collection as $_product){
			$stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);
			$products = array();
			$_product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($_product->getId());
			$products['entity_id'] = $_product->getEntityId();
			$products['short_description'] = $_product->getShortDescription();
			$products['name'] = $_product->getName();
			$products['image_url'] = $_product->getImageUrl();
			$products['regular_price'] = (string) $_product->getPrice();
			$products['final_price'] = (string) $_product->getFinalPrice();
			$products['is_saleable'] = $stock->getIsInStock();
			/** Fetch Custom Option **/
		            $options = Mage::getModel('catalog/product_option')->getProductOptionCollection($_product);
		            $_custom_options = array();
		            foreach ($options as $option) {
		                $_custom_options[$option->getOptionId()] = array('name' => $option->getDefaultTitle(),
		                                                                'type' => $option->getType());
		                $values = Mage::getSingleton('catalog/product_option_value')->getValuesCollection($option);
		                $i = 1;
		                foreach ($option->getValues() as  $value) {
		                    $_custom_options[$option->getOptionId()]['value'][] = array('title' => $value->getTitle(),
		                                                                                'price' => $value->getPrice(),'price_type' => $value->getPriceType(),);
		                    
		                    $i++;
		                }
		            }
		            /** End of Fetch Custom Option **/

			$newproducts[] = $products;
		}
	    return $newproducts;
	}
	
	//get newarrivals products
	public function featuredproducts($storeId){
		$featuredproducts = array();
		//$filter = 'new';
		$collection = Mage::getSingleton('catalog/product')->getCollection()->setStoreId($storeId);
		$collection->addAttributeToSelect('*');
		$collection->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
		$collection->addAttributeToFilter('featured', 1);

		$visibility = array(
				Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
				Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG
			);
		$collection->addAttributeToFilter('visibility', $visibility);
		//$collection->setOrder('entity_id','DESC');

		
		Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
		$collection->setPage(1, 10);
        
		foreach($collection as $_product){
			$stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);
			$products = array();
			$_product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($_product->getId());
			$products['entity_id'] = $_product->getEntityId();
			$products['short_description'] = $_product->getShortDescription();
			$products['name'] = $_product->getName();
			$products['image_url'] = $_product->getImageUrl();
			$products['regular_price'] = (string) $_product->getPrice();
			$products['final_price'] = (string) $_product->getFinalPrice();
			$products['is_saleable'] = $stock->getIsInStock();
			/** Fetch Custom Option **/
		            $options = Mage::getModel('catalog/product_option')->getProductOptionCollection($_product);
		            $_custom_options = array();
		            foreach ($options as $option) {
		                $_custom_options[$option->getOptionId()] = array('name' => $option->getDefaultTitle(),
		                                                                'type' => $option->getType());
		                $values = Mage::getSingleton('catalog/product_option_value')->getValuesCollection($option);
		                $i = 1;
		                foreach ($option->getValues() as  $value) {
		                    $_custom_options[$option->getOptionId()]['value'][] = array('title' => $value->getTitle(),
		                                                                                'price' => $value->getPrice(),'price_type' => $value->getPriceType(),);
		                    
		                    $i++;
		                }
		            }
		            /** End of Fetch Custom Option **/

			$featuredproducts[] = $products;
		}
	    return $featuredproducts;
	}

    public function bannerSlider($storeId){
		$sliders = Mage::getModel('revslider/slider')->getCollection();
    	$media_url =  Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
    	foreach ($sliders as $slider) {
			 $dateFrom = $slider->getData('date_from');
			$dateTo = $slider->getData('date_to');
			/* @var $date Mage_Core_Model_Date */
			$date = Mage::getModel('core/date');
			if ($dateFrom && $date->date('Y-m-d') < $dateFrom) continue;
			if ($dateTo && $date->date('Y-m-d') > $dateTo) continue;
    		$slideData = array();
			$title=$slider->getTitle();
			$slideData[$title]['title'] = $title;
			$slideData[$title]['id'] = $slider->getId();
			$slideData[$title]['status'] = $slider->getStatus();
			$slides = $this->getAllSlides(true, $slider->getId());
			foreach ($slides as $slide) {
				$dateFrom = $slide->getData('date_from');
                $dateTo = $slide->getData('date_to');
                $date = Mage::getModel('core/date');
                if ($dateFrom && $date->date('Y-m-d') < $dateFrom) continue;
                if ($dateTo && $date->date('Y-m-d') > $dateTo) continue;
				$slideData[$title]['slide'][$slide->getId()]['title']= $slide->getTitle();
				//$slideData[$title]['slide'][$slide->getId()]['state']= $slide->getState();
				$slideData[$title]['slide'][$slide->getId()]['image_url']= $media_url.$slide->getImageUrl();
				$slideData[$title]['slide'][$slide->getId()]['slide_order']= $slide->getSlideOrder();
				$slideData[$title]['slide'][$slide->getId()]['id']= $slide->getId();
			}
    		$banner_slider[] = $slideData;
    	}
		return $banner_slider;
    }
	public function getAllSlides($published=false, $filterIds=null){
        $collection = Mage::getModel('revslider/slide')
            ->getCollection()
            ->addSliderFilter($this)
            ->setOrder('slide_order', 'asc');

		if ($filterIds) $collection->addFieldToFilter('slider_id', array('in' =>$filterIds));

        $slides = array();

        foreach ($collection as $slide){
            $id = $slide->getId();
            $slideOrder = $slide->getData('slide_order');
            $layers = $slide->getLayers();
            $slide->setData(Mage::helper('core')->jsonDecode($slide->getParams()));
            $slide->setData('slide_order', $slideOrder);
            $slide->setId($id);
            if ($published){
                if ($slide->getData('state') == 'published'){
                    $slides[] = $slide;
                }
            }else{
                $slides[] = $slide;
            }
        }
        return $slides;
    }
	

}