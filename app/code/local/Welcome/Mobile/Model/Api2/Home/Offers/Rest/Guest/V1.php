<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Welcome_Mobile_Model_Api2_Home_Offers_Rest_Guest_V1 extends Welcome_Mobile_Model_Api2_Home_Offers
{
	/**
	 * Retrieve list of Home page offers.
	 *
	 * @return array
	 */
	protected function _retrieveCollection()
	{
		$homeproducts = array();
		$post = $this->getRequest()->getParams();
        if($post['lang']){
        	$store = Mage::getModel('core/store')->load($post['lang']);
        	$storeId = $store->getStoreId();
			
        	$websiteId = $store->getWebsiteId();
        }
        else{
        	$storeId = 1;
        }
		
        $key = "api_home_home_offers_".$storeId;
        try {
            $mobileHelper = Mage::helper('mobile');
            //$redisClient = $mobileHelper->getRedisClient();
            //$redisClient->remove($key);
            //$homeProducts = unserialize($redisClient->load($key));
            
            //if($homeProducts){
                //Mage::log('Hit - Homeproducts List Api',null,'api_record.log');
                //return $homeProducts;
            //}
        } catch (Exception $e) {
            Mage::log('Error in Cache',null,'api_record.log');
        }
        Mage::log('Miss - Homeproducts List Api',null,'api_record.log');
        Mage::app()->setCurrentStore($storeId);
		$offers = $this->getAllProductsAndOffers($storeId);
		$homeproducts['offers'][]=$offers;

       	//$redisClient->save(serialize($homeproducts),$key, array('api_cache', 'home_product'), 60*60*24);
	    return $homeproducts;
	}
	

	public function getAllProductsAndOffers($storeId){
        $allProductsAndOffers = array();
        include 'simple_html_dom.php';
        $block = Mage::getModel('cms/block')->setStoreId($storeId)->load('home_all_categories');
		$media_url =  Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
		$title = $block->getTitle();
		$content = $block->getContent();
		$html = str_get_html($content);

		$isActive = $block->getIsActive();
        
		if($isActive){
			$allProducts = $html->find('div[id=allProducts]', 0);
            $allProductsHtml = $allProducts->outertext;
            $test = str_get_html($allProductsHtml);
            $allProductsImageUrl = array();
            $i = 0;
            foreach($test->find('div[class=row]') as $element) {
                $test = str_get_html($element->outertext);
                foreach($test->find('div[class=col-md-2]') as $innerElement) {
                    $urlPath = $innerElement->find('img[class=img-responsive]', 0)->src;
                    $urlPathArray = explode('"',$urlPath);

                    $allProductsImageUrl[$i]['url'] = $media_url.$urlPathArray[1];
                    $allProductsImageUrl[$i]['title'] = $innerElement->plaintext;
                    $i++;

                }
            }

            //Get All Offer
            $allOffers = $html->find('div[id=ourBrands]', 0);
            $allOffersHtml = $allOffers->outertext;
            $offersObject = str_get_html($allOffersHtml);
            $allOffersImageUrl = array();
            $i = 0;
            foreach($offersObject->find('div[class=col-md-4]') as $element) {
                $urlPath = $element->find('img[class=img-responsive]', 0)->src;
                $urlPathArray = explode('"',$urlPath);
                $allOffersImageUrl[$i]['url'] = $media_url.$urlPathArray[1];
                $i++;
            }
            $allProductsAndOffers['allProducts'] = $allProductsImageUrl;
            $allProductsAndOffers['allOffers'] = $allOffersImageUrl;
			return $allProductsAndOffers;
		}else{
			return $allProductsAndOffers;
		}
    }
}