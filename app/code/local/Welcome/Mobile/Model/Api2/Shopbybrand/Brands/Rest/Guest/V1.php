<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Welcome_Mobile_Model_Api2_Shopbybrand_Brands_Rest_Guest_V1 extends Welcome_Mobile_Model_Api2_Shopbybrand
{
    
    protected function _retrieveCollection()
    {
        $post = $this->getRequest()->getParams();
        if ($post) {
            if($post['lang']){
                $store = Mage::getModel('core/store')->load($post['lang']);
                $storeId = $store->getStoreId();
                $websiteId = $store->getWebsiteId();
                $lang = (strchr($post['lang'], 'ar') == false) ? 1 : 3 ;
            }
            else{
                $storeId = 1;
                $websiteId = 1;
                $lang = 1; 
            }
            // Load the required attribute
            $attributeCode = "brand";
            $attributeId = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($attributeCode)->getFirstItem()->getAttributeId();
            $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);

            // Get the Admin Store View (default) attribute options
            $attributeOptions = Mage::getResourceModel('eav/entity_attribute_option_collection')
                ->setAttributeFilter($attributeId)
                ->setStoreFilter($storeId)
                ->setPositionOrder()
                ->load()
                ->toOptionArray();

            // Save the attribute options as an array where the key is the option ID and the value is the label
            $attributeOptionsNew = array();
            $i = 0;
            foreach($attributeOptions AS $attributeOption) {
                $attributeOptionsNew[$i]['code'] = $attributeOption['value'];
                $attributeOptionsNew[$i]['label'] = $attributeOption['label'];
                $i++;
            }
            //$attributeOptions = $attributeOptionsNew;
            return $attributeOptionsNew;
        }
    }
}