<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Welcome_Mobile_Model_Api2_Shopbybrand_Products_Rest_Guest_V1 extends Welcome_Mobile_Model_Api2_Shopbybrand_Products
{
    
    protected function _retrieveCollection()
    {
        $post = $this->getRequest()->getParams();
        if ($post) {
            if($post['lang']){
                $store = Mage::getModel('core/store')->load($post['lang']);
                $storeId = $store->getStoreId();
                $websiteId = $store->getWebsiteId();
                $lang = (strchr($post['lang'], 'ar') == false) ? 1 : 3 ;
            }
            else{
                $storeId = 1;
                $websiteId = 1;
                $lang = 1; 
            }
            if(isset($post['brand_id']) && $post['brand_id'] > 0){
                $brandId = $post['brand_id'];
            }else{
                return array();
            }
            
            $collection = Mage::getModel('catalog/product')->getCollection();
            $collection->addAttributeToSelect('*');  
            //$collection->addAttributeToSelect('orig_price');    

            //filter for products whose orig_price is greater than (gt) 100
            $collection->addFieldToFilter(array(
                array('attribute'=>'brand','eq'=>$brandId),
            ));
            $productsArr = array();
            /** @var Mage_Catalog_Model_Product $product */
            foreach ($collection as $product) {
                //$this->_setProduct($product);
                $pro = $this->_prepareProductForCollection($product);
                $productsArr[] = $pro;
            }
            $catpro['products'][] = $productsArr;
            $catpro['page_info'][] = array('total_pages'=>1, 'total_items'=>count($productsArr));
            return $catpro;
        }
    }
    
    /**
     * Add special fields to product get response
     *
     * @param Mage_Catalog_Model_Product $product
     */
    protected function _prepareProductForCollection(Mage_Catalog_Model_Product $product)
    {
        /** @var $productHelper Mage_Catalog_Helper_Product */
        $productHelper = Mage::helper('catalog/product');
        $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
		//$rating = Mage::getModel('review/review_summary')->setStoreId($this->_getStore()->getId())->load($product->getId());
		//$reviewModel = Mage::getModel('review/review');
        $product = Mage::getModel('catalog/product')->setStoreId($this->_getStore()->getId())->load($product->getId());
        $categoryId = $this->getRequest()->getParam('category_id');
        //$celebrityCategory = Mage::helper('celebrity')->getCelebrityCategoryIds();
        //$max = unserialize($product->getMax());
        //$maxorder = unserialize($product->getMaxorder());
        $productData = array();
        // calculate prices
        $finalPrice = $product->getFinalPrice();
        //get product price and name based on celebrity
        /*if($this->getRequest()->getParam('category_id')){
            $category = Mage::getModel('catalog/category')->load(95);
            $celebrityCat = explode(',', $category->getChildren());
            $categoryId = $this->getRequest()->getParam('category_id');
            //$categoryIds = Mage::helper('celebrity')->getCelebrityCategoryIds();
            //$splname = ((in_array($categoryId, $categoryIds) || in_array($categoryId, $celebrityCat)) && $product->getSpecialName()) ? $product->getSpecialName() : $product->getName();
            $splname = $product->getName();
            $data = unserialize($product->getCelebritySpecialPrice());
            $adnumber = unserialize($product->getCelebrityAdNumber());
            $finalPrice = isset($data[$categoryId]) ? (float)$data[$categoryId] : $product->getFinalPrice();
            $label = isset($adnumber[$categoryId]) ? (int)$adnumber[$categoryId] : null;
        }
        else{
            $label = null;
            $finalPrice = $product->getFinalPrice();
            $splname = $product->getName();
        }*/
        // $product->setWebsiteId(1);
        // customer group is required in product for correct prices calculation
        $product->setCustomerGroupId($this->_getCustomerGroupId());
        	
        $productData['entity_id'] = $product->getEntityId();
        $productData['short_description'] = $product->getShortDescription();
        $productData['name'] = $product->getName();
        $productData['regular_price'] = (string) $product->getPrice();
        $productData['final_price'] = (string) $finalPrice;
        //$productData['exclusive'] = (int) $product->getExclusive();
        //$productData['exclusive_celebrity'] = $product->getExclusiveOnCelebrity();
		//$productData['rating'] = $rating->getRatingSummary();
		//$productData['total_reviews_count'] = $reviewModel->getTotalReviews($product->getId(), true,
                //$this->_getStore()->getId());
        //if($label!=null){
           //$productData['product_label'] = $label; 
        //}
        $productData['is_saleable'] = $stock->getIsInStock();
        /*if(in_array($categoryId, $celebrityCategory) && $stock->getIsInStock()==1){
            if(is_array($max) && is_array($maxorder)){
                if (isset($max[$categoryId])) {
                    if($maxorder[$categoryId] >= $max[$categoryId]){
                       $productData['is_saleable'] = 0;
                    }
                }               
            }
        }*/
        //$productData['image_url'] = (string) Mage::helper('catalog/image')->init($product, 'image');
        $productData['image_url'] = $product->getImageUrl();
        /** Fetch Custom Option **/
        $options = Mage::getModel('catalog/product_option')->getProductOptionCollection($product);
        $_custom_options = array();
        foreach ($options as $option) {
            $_custom_options[$option->getOptionId()] = array('name' => $option->getDefaultTitle(),
                                                            'type' => $option->getType());
            $values = Mage::getSingleton('catalog/product_option_value')->getValuesCollection($option);
            $i = 1;
            foreach ($option->getValues() as  $value) {
                $_custom_options[$option->getOptionId()]['value'][] = array('title' => $value->getTitle(),
                                                                            'price' => $value->getPrice(),'price_type' => $value->getPriceType(),);
           
                $i++;
            }
        }
        /** End of Fetch Custom Option **/
        $productData['has_custom_options'] = count($options) > 0;
        $productData['custom_options'] = (!empty($_custom_options))?$_custom_options:'';
        return $productData;
    }
      
    protected function _getCustomerGroupId()
    {
        return null;
    }
}