<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Welcome_Mobile_Model_Api2_Categorylist_Rest_Guest_V1 extends Welcome_Mobile_Model_Api2_Categorylist
{
	/**
	 * Retrieve list of category list.
	 *
	 * @return array
	 */
	protected function _retrieveCollection()
	{
		$post = $this->getRequest()->getParams();
	    $category = ($this->getRequest()->getParam('cat_id')) ? $this->getRequest()->getParam('cat_id') : 0;
	    //$storeId = ($post['lang'] == 'ar') ? 3 : 1;
	    if($post['lang']){
            $store = Mage::getModel('core/store')->load($post['lang']);
            $storeId = $store->getStoreId();
            $websiteId = $store->getWebsiteId();
        }
        else{
        	$storeId = 1;
        }
        if($category){
        	$category = Mage::getModel('catalog/category')->setStoreId($storeId)->load($category);
        	$level = $category->getLevel();
        	$breadcrumb = array();
        	$breadcr['name'] = $category->getName();
        	$breadcrumb[] = $breadcr;
        	$parent = Mage::getModel('catalog/category')->setStoreId($storeId)->load($category->getParentId());
        	while($parent->getLevel()>=2){
        		$breadcr['name'] = $parent->getName();
        		$breadcr['category_id'] = $parent->getId();
        		$parent = Mage::getModel('catalog/category')->setStoreId($storeId)->load($parent->getParentId());
        		$breadcrumb[] = $breadcr;
        	}

        }else{
        	$level = 2;
        }
	    return $this->getTreeCategories($category, $storeId, $level, $breadcrumb);

	}

	protected function getTreeCategories($parentId, $storeId, $level = null, $breadcrumb = ''){

		$cur_category = array();
	    $node = array();
		if($parentId !=0 && is_object($parentId)){
			$category = $parentId;
			$cur_category['breadcrumb'][] = array_reverse($breadcrumb);
			$subcats = $category->getChildren();
			$cur_category['category_id'] = $category->getId();
		    $cur_category['name'] = $category->getName();
		    $cur_category['parent_id'] = $category->getParentId();
		    $cur_category['child_id'] = $subcats;
		    $cur_category['active'] = $category->getIsActive();
		    $cur_category['level'] = $category->getLevel();
		    $cur_category['position'] = $category->getPosition();
		    if($level && $level == $category->getLevel()){
		    	if($subcats == ''){
		            $cur_category['children'] = $subcats;
		        }
		        else{
		        	$allchild = array();
		        	$subcats = explode(',', $subcats);
		        	foreach ($subcats as $sub=>$subvalue)
		    		{
		    			$sub = Mage::getModel('catalog/category')->setStoreId($storeId)->load((int)$subvalue);
		    			$subchild = $sub->getChildren();
		    			$node['category_id'] = $sub->getId();
					    $node['name'] = $sub->getName();
					    $node['parent_id'] = $sub->getParentId();
					    $node['active'] = $sub->getIsActive();
					    $node['child_id'] = $subchild;
					    $node['level'] = $sub->getLevel();
					    $node['thumbnail'] = ($sub->getThumbnail()) ? Mage::getBaseUrl('media').'catalog/category/'.$sub->getThumbnail() : '';
					    $node['position'] = $sub->getPosition();
					    $allchild[] = $node;
		        	}
		        	$cur_category['children'][] = $allchild;
		        }	
		    }  
		}else{
			$_all_categories = Mage::getModel('catalog/category')->getCollection()
					->setStoreId($storeId)
	                ->addAttributeToSelect('*')
	                ->addAttributeToFilter('is_active', true)
	                ->addAttributeToFilter('entity_id', array('neq'=>6))
	                ->addAttributeToFilter('entity_id', array('neq'=>95))
	                ->addAttributeToFilter('include_in_menu', true);
		    if($level){
		    	$_all_categories->addAttributeToFilter('level', array('eq'=>$level));
		    }
		    if($parentId){
		    	$_all_categories->addAttributeToFilter('parent_id', array('eq' => $parentId));
		    }
			
			$_all_categories->addAttributeToSort('position');
	    
		    foreach ($_all_categories as $category)
		    {
		    	$subcats = $category->getChildren();
		    	$node['category_id'] = $category->getId();
			    $node['name'] = $category->getName();
			    $node['parent_id'] = $category->getParentId();
			    $node['child_id'] = $subcats;
			    $node['active'] = $category->getIsActive();
			    $node['level'] = $category->getLevel();
			    $node['thumbnail'] = ($category->getThumbnail()) ? Mage::getBaseUrl('media').'catalog/category/'.$category->getThumbnail() : '';
			    $node['position'] = $category->getPosition();
			    if($level && $level == $category->getLevel()){
			    	if($subcats == ''){
			            $node['children'] = $subcats;
			        }
			        else{
			        	$node['children'] = $this->getTreeCategories($category->getId(), $storeId);
			        }	
			    }   
		        $cur_category[] = $node;
		    }
		}
	    return $cur_category;
	}
}
