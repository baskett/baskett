<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Welcome_Mobile_Model_Api2_Customer_Newshippingaddress_Rest_Guest_V1 extends Welcome_Mobile_Model_Api2_Customer_Newshippingaddress
{
    
    protected function _create()
    {
        $post = $this->getRequest()->getBodyParams();
        if ($post) {
            if($post['lang']){
                $store = Mage::getModel('core/store')->load($post['lang']);
                $storeId = $store->getStoreId();
                $websiteId = $store->getWebsiteId();
                $lang = (strchr($post['lang'], 'ar') == false) ? 1 : 3 ;
            }
            else{
                $websiteId = 1;
                $storeId = 19;
                $lang = 1;
            }
				Mage::log('Request ----',null,'shippingaddapi.log');
				Mage::log($post,null,'shippingaddapi.log');
				//Mage::log('Response ----',null,'shippinglistapi.log');
				//Mage::log($custAddress,null,'shippinglistapi.log');
            //exit;
            $customer_id = $post['customer_id'];
            $decrypt = Mage::getSingleton('core/encryption');
            //$customer_id = base64_decode($decrypt->decrypt($customer_id));
            $customer_id = pack("H*", $customer_id);
            $customer_id = $decrypt->decrypt($customer_id);
            $shipping = array();
            $customer = Mage::getSingleton('customer/customer')->setWebsiteId($websiteId)->load($customer_id);
            if($customer->getId()){
                $customerAddress = Mage::getModel('customer/address');
                $defshipping = $customer->getDefaultShipping();
                try{
                    Mage::app()->setCurrentStore($storeId);
                    $address = Mage::getModel("customer/address");
                    $post['region'] = $post['region']?$post['region']:'region';
                    $post['postcode'] = $post['postcode']?$post['postcode']:'13003';
                    $post['telephone'] = $post['telephone']?$post['telephone']:$post['mobile'];
                    $post['city'] = $post['city'] ? $post['city'] : 'city';
                    $post['country_id'] = $post['country_id'] ? $post['country_id'] : 'KW';
                    $address->addData($post);
                    $customer->addAddress($address);
                    $address->setCustomerId($customer->getId());
                    $customer->save();
                    $address->save();
                    $msgsuccess = ($lang == 3) ? Mage::helper('customer')->__('تم حفظ العناوين') : Mage::helper('customer')->__('The Address has been saved');
                    //$customer = Mage::getModel('customer/customer')->setWebsiteId($websiteId)->load($customer_id);
                    $primaryShipping = $customer->getDefaultShippingAddress();
					//echo "<pre>";print_r($primaryShipping);die;
                    if(!isset($primaryShipping['entity_id'])){
                        $primaryShipping = Mage::getModel("customer/address")->load($address->getId());
                    }
					//echo "<pre>";print_r($primaryShipping);die;
                    //if($primaryShipping->getCountryId() == $primaryShipping->getDefaultShippingCountry()){
					
					/* send back all existing shipping address begin */
					//get primary shipping address
					$custAddress = array();
					if(isset($primaryShipping['entity_id'])) {
						$custAddress[0] = array (
							'customer_address_id'=> $primaryShipping->getId(),
							'firstname' => $primaryShipping->getFirstname(),
							'middlename' => $primaryShipping->getMiddlename(),
							'lastname' => $primaryShipping->getLastname(),
							'company' => $primaryShipping->getCompany(),
							'telephone' => $primaryShipping->getTelephone() ? $primaryShipping->getTelephone() : '',
							'governorate' => $primaryShipping->getGovernorate(),
							'area' => $primaryShipping->getArea() ? $primaryShipping->getArea() : '',
							'block' => $primaryShipping->getBlock() ? $primaryShipping->getBlock() : '',
							'street' => $primaryShipping->getStreet1() ? $primaryShipping->getStreet1() : '',
							'avenue' => $primaryShipping->getAvenue() ? $primaryShipping->getAvenue() : '',
							'building' => $primaryShipping->getBuilding(),
							'appartment' => $primaryShipping->getAppartment(),
							'tsh_number' => $primaryShipping->getTshNumber(),
							'mobile' => $primaryShipping->getMobile(),
							'country_id' => $primaryShipping->getCountryId(),
							'is_default_shipping' => (int)$primaryShipping->getIsDefaultShipping(),
						);
					}
					//get additional shipping address
					if(count($customer->getAdditionalAddresses())){
						$i = 1;
						foreach($customer->getAdditionalAddresses() as $address1){
								$custAddress[$i] = array (
								'customer_address_id'=> $address1->getId(),
                                'firstname' => $address1->getFirstname(),
                                'middlename' => $address1->getMiddlename(),
                                'lastname' => $address1->getLastname(),
                                'company' => $address1->getCompany(),
                                'telephone' => $address1->getTelephone() ? $address1->getTelephone() : '',
                                'governorate' => $address1->getGovernorate(),
                                'area' => $address1->getArea() ? $address1->getArea() : '',
                                'block' => $address1->getBlock() ? $address1->getBlock() : '',
                                'street' => $address1->getStreet1() ? $address1->getStreet1() : '',
                                'avenue' => $address1->getAvenue() ? $address1->getAvenue() : '',
                                'building' => $address1->getBuilding(),
                                'appartment' => $address1->getAppartment(),
                                'tsh_number' => $address1->getTshNumber(),
                                'mobile' => $address1->getMobile(),
                                'country_id' => $address1->getCountryId(),
								'is_default_shipping' => (int)$address1->getIsDefaultShipping(),
								);
							$i++;
						}
					}
                    //}
                    $json = array('status' => 'Success', 'message' => $msgsuccess, 'shipping_address'=> $custAddress);
                    echo json_encode($json);
                    exit();
                }catch(Exception $e){
                    $msgerror = $e->getMessage();
                    $json = array('status' => 'error', 'message' => $msgerror);
                    echo json_encode($json);
                    exit();
                }
            }
            else{
                $msgerror = ($lang == 3) ? Mage::helper('customer')->__('العميل غير صالح') :  Mage::helper('customer')->__('Invalid customer');
                $json = array('status' => 'error', 'message' => $msgerror);
                echo json_encode($json);
                exit();
            }
        }
    }

    /* Images */

    protected $_mimeTypes = array(
        'image/jpg'  => 'jpg',
        'image/jpeg' => 'jpg',
        'image/gif'  => 'gif',
        'image/png'  => 'png'
    );

    protected function _getFileName($data)
    {
        $fileName = 'image';
        if (isset($data['file_name']) && $data['file_name']) {
            $fileName = $data['file_name'];
        }
        $fileName .= '.' . $this->_getExtensionByMimeType($data['file_mime_type']);
        return $fileName;
    }

    /**
     * Retrieve file extension using MIME type
     *
     * @throws Mage_Api2_Exception
     * @param string $mimeType
     * @return string
     */
    protected function _getExtensionByMimeType($mimeType)
    {
        if (!array_key_exists($mimeType, $this->_mimeTypes)) {
            $this->_critical('Unsuppoted image MIME type', Mage_Api2_Model_Server::HTTP_BAD_REQUEST);
        }
        return $this->_mimeTypes[$mimeType];
    }


    protected function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
