<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Welcome_Mobile_Model_Api2_Customer_Editshippingaddress_Rest_Guest_V1 extends Welcome_Mobile_Model_Api2_Customer_Editshippingaddress
{
    
    protected function _create()
    {
        $post = $this->getRequest()->getBodyParams();
        if ($post) {
            if($post['lang']){
                $store = Mage::getModel('core/store')->load($post['lang']);
                $storeId = $store->getStoreId();
                $websiteId = $store->getWebsiteId();
                $lang = (strchr($post['lang'], 'ar') == false) ? 1 : 3 ;
            }
            else{
                $storeId = 1;
                $websiteId = 1;
                $lang = 1;
            }
            $customer_id = $post['customer_id'];
            $decrypt = Mage::getSingleton('core/encryption');
            //$customer_id = base64_decode($decrypt->decrypt($customer_id));
            $customer_id = pack("H*", $customer_id);
            $customer_id = $decrypt->decrypt($customer_id);
            $shipping = array();
            $customer = Mage::getSingleton('customer/customer')->setWebsiteId($websiteId)->load($customer_id);
            if($customer->getId()){
                $customerAddress = Mage::getModel('customer/address');
                $address = $customerAddress->load($post['address_id']);
                if(($address->getEntityId()>0) && ($address->getParentId()==$customer->getId())){

                    if(isset($post['qa_city'])){
                        $post['city'] = $post['qa_city'];
                    }

                    try{
                        $details  = array();
                        $details[0] = $post['address_line_1'];
                        $details[1] = $post['address_line_2'];
                        $post['street'] = $details;
                        if($websiteId != 1 && $post['country_id'] != 'KW'){
                            $post['lastname'] = '.';
                        }
                        if($post['is_default_shipping'] && $websiteId == 1 && $post['country_id'] == 'KW'){
                            $post['is_default_shipping'] = 1;
                        }
                        /* if(strchr($post['lang'], strtolower($post['country_id']))){
                            foreach($customer->getAddresses() as $address1){
                                if(strchr($post['lang'], strtolower($address1->getCountryId())) && $post['address_id']!=$address1->getId()){
                                    if(isset($post['is_default_shipping'])){
                                        $post['default_shipping_country'] = $post['country_id'];
                                        unset($post['is_default_shipping']);
                                        $address1->setDefaultQatarShipping(0);
                                        $address1->save();
                                    } 
                                }
                            }
                            if(isset($post['is_default_shipping'])){
                                unset($post['is_default_shipping']);
                            }
                        } */
                         $post['time_slot'] = $post['time_slot'];
                        //$address = Mage::getModel("customer/address");
                        $address->addData($post);
                        $address->setCustomerId($customer->getId());
                        //$customer->save();
                        $address->save();
                        $msgsuccess = ($lang == 3) ? Mage::helper('customer')->__('تم تحديث العناوين') : Mage::helper('customer')->__('The Address has been updated');
                        $json = array('status' => 'Success', 'message' => $msgsuccess);
                        echo json_encode($json);
                        exit();
                    }catch(Exception $e){
                        $msgerror = $e->getMessage();
                        $json = array('status' => 'error', 'message' => $msgerror);
                        echo json_encode($json);
                        exit();
                    }
                }
                else{
                    $message = ($lang == 3) ? Mage::helper('customer')->__('ID عنوان غير صالح') : Mage::helper('customer')->__('Invalid Address ID');
                    $json = array('status' => 'error', 'message' => $message);
                    echo json_encode($json);
                    exit();
                }
            }
            else{
                $msgerror = ($lang == 3) ? Mage::helper('customer')->__('العميل غير صالح') : Mage::helper('customer')->__('Invalid customer');
                $json = array('status' => 'error', 'message' => $msgerror);
                echo json_encode($json);
                exit();
            }
        }
    }


    /* Images */

    protected $_mimeTypes = array(
        'image/jpg'  => 'jpg',
        'image/jpeg' => 'jpg',
        'image/gif'  => 'gif',
        'image/png'  => 'png'
    );

    protected function _getFileName($data)
    {
        $fileName = 'image';
        if (isset($data['file_name']) && $data['file_name']) {
            $fileName = $data['file_name'];
        }
        $fileName .= '.' . $this->_getExtensionByMimeType($data['file_mime_type']);
        return $fileName;
    }

    /**
     * Retrieve file extension using MIME type
     *
     * @throws Mage_Api2_Exception
     * @param string $mimeType
     * @return string
     */
    protected function _getExtensionByMimeType($mimeType)
    {
        if (!array_key_exists($mimeType, $this->_mimeTypes)) {
            $this->_critical('Unsuppoted image MIME type', Mage_Api2_Model_Server::HTTP_BAD_REQUEST);
        }
        return $this->_mimeTypes[$mimeType];
    }


    protected function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
