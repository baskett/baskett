<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Welcome_Mobile_Model_Api2_Customer_Updatecart_Rest_Guest_V1 extends Welcome_Mobile_Model_Api2_Customer_Updatecart
{
   protected function _create()
    {
        $post = $this->getRequest()->getBodyParams();
        $inclue = 0;
        if ($post) {
            $customer_id = $post['customer_id'];
            //$storeId = ($post['lang'] == 'ar') ? 3 : 1;
            if($post['lang']){
                $store = Mage::getModel('core/store')->load($post['lang']);
                $storeId = $store->getStoreId();
                $websiteId = $store->getWebsiteId();
                $lang = (strchr($post['lang'], 'ar') == false) ? 1 : 3 ;
            }
            else{
                $storeId = 19;
                $websiteId = 1;   
                $lang = 1;
            }
            $website = Mage::getModel('core/website')->load($websiteId);
            foreach ($website->getGroups() as $group) {
                $stores = $group->getStores();
                foreach ($stores as $store) {
                    $store_id[] = $store->getId();
                }
            }
            Mage::app()->setCurrentStore($storeId);
            $item_id = $post['item_id'];
            $qty = $post['quantity'];
            $decrypt = Mage::getSingleton('core/encryption');
            //$customer_id = base64_decode($decrypt->decrypt($customer_id));
            $customer_id = pack("H*", $customer_id);
            $customer_id = $decrypt->decrypt($customer_id);
            $customer = Mage::getSingleton('customer/customer')->setWebsiteId($websiteId)->load($customer_id);
            if($customer->getId()){
                try{
                    if(!isset($item_id) || $item_id == '') {
                        throw new Exception(Mage::helper('customer')->__('Invalid  ItemId.'));
                    }
                    /*if($qty > 6){
                        $inclue = 1;
                        if($lang == 1){
                                Mage::throwException('Maximum number of qty per item is 6.');
                        }
                        else {
                            Mage::throwException('الحد الأقصى لعدد الكمية لكل بند هو 6.');
                        }
                    }*/
                    $quote = Mage::getModel('sales/quote')->getCollection()->addFieldToFilter('store_id', array('in', $store_id))->addFieldToFilter('customer_id', $customer->getId())
                    		->addFieldToFilter('is_active', 1)->setOrder('entity_id', 'desc')->getFirstItem();
                   	$item = $quote->getItemById($item_id);

                   	if($qty>0){
                   		$item->setQty($qty);	
                   	}
                   	else{
                   		$quote->removeItem($item->getItemId());
                   	}
                    $quote->collectTotals();
                    $shipmethods = Mage::getSingleton('shipping/config')->getActiveCarriers();
                    $shipping_method = array();
                    foreach($shipmethods as $carrierCode => $carrierModel){ 
                        $shipping = array();                           
                       if( $carrierMethods = $carrierModel->getAllowedMethods() )
                       {
                           foreach ($carrierMethods as $methodCode => $method)
                           {    
                                $code = $carrierCode.'_'.$methodCode;
                           }
                           $ship_price = Mage::getStoreConfig('carriers/'.$carrierCode.'/price', $storeId);
                       }
                    }
                    if($websiteId != 1){
                        $cur_code = Mage::app()->getStore()->getCurrentCurrencyCode();
                        $to_code = 'KWD';
                        $quote_price = $ship_price + $quote->getBaseGrandTotal();
                        $quote_price = Mage::helper('directory')->currencyConvert($quote_price, $cur_code, $to_code);
                    }
                    else{
                        $quote_price = $quote->getBaseGrandTotal() + $ship_price;
                    }
                    //restrict max order amount which is specified in admin
                    /*$max_amount_enable = Mage::getStoreConfig('sales/inchoo_maxorderamount/active');
                    $max_amount = Mage::getStoreConfig('sales/inchoo_maxorderamount/single_order_top_amount');
                    $max_amount_message = Mage::getStoreConfig('sales/inchoo_maxorderamount/single_order_top_amount_msg');
                    if($max_amount_enable && $quote_price > $max_amount){
                        $inclue = 1;
                        if($websiteId == 1){
                            if($lang == 1){
                                throw new Exception(Mage::helper('customer')->__('No single order allowed with amount over '.$max_amount.' KD'));
                            }
                            else {
                                throw new Exception(Mage::helper('customer')->__('أي أمر واحد يسمح مع مبلغ أكثر '.$max_amount.' KD'));
                            }
                        }else{
                            if($lang == 1){
                                throw new Exception(Mage::helper('customer')->__('No single order allowed with amount over 4800 QR'));
                            }
                            else {
                                throw new Exception(Mage::helper('customer')->__('أي أمر واحد يسمح مع مبلغ أكثر 4800 QR'));
                            }
                        }

                    }*/
                    $quote->save();
                    $msgsuccess = ($lang == 3) ? Mage::helper('customer')->__('تم تحديث المنتج') : Mage::helper('customer')->__('The product has been updated');
                    $itemDet = $this->getAvailableCartItems($storeId, $customer->getId(), $store_id);
                    $info = array('status' => 'Success', 'message' => $msgsuccess);
                    echo json_encode(array_merge($info, array('items'=>$itemDet)));
                    exit();
                }catch(Exception $e){
                    $addmsg = ($lang == 3) ? Mage::helper('customer')->__('لا يمكن تحديث المنتج') : Mage::helper('customer')->__('Product cannot be updated');
                    $msgerror = $e->getMessage().' '.$addmsg;
                    $info = array('status' => 'error', 'message' => $msgerror);
                    $itemDet = array();
                    if($inclue){
                        $itemDet =$this->getAvailableCartItems($storeId, $customer->getId(), $store_id);
                    }
                    echo json_encode(array_merge($info,array('items'=>$itemDet)));
                    exit();
                }
            }
            else{
                $msgerror = ($lang == 3) ? Mage::helper('customer')->__('العميل غير صالح') : Mage::helper('customer')->__('Invalid customer');
                $json = array('status' => 'error', 'message' => $msgerror);
                echo json_encode($json);
                exit();
		  }
			
	   }
    }
    public function getAvailableCartItems($storeId, $customer_id, $store_id){

        $itemDet = array();
        $quoteFinal = Mage::getModel('sales/quote')->getCollection()->addFieldToFilter('store_id', array('in', $store_id))->addFieldToFilter('customer_id', $customer_id)
        ->addFieldToFilter('is_active', 1)->setOrder('entity_id', 'desc')->getFirstItem();
        if($quoteFinal->getId()){
           $quoteCollection = $quoteFinal->getAllVisibleItems();
            //get celebrities children
            //$category = Mage::getModel('catalog/category')->load(95);
            //$celebrityCat = explode(',', $category->getChildren());
            //$categoryIds = Mage::helper('celebrity')->getCelebrityCategoryIds();
            foreach ($quoteCollection as $_item)
            {
                $_product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($_item->getProductId());
                //$splname = ((in_array($_item['category_id'], $celebrity) || in_array($_item['category_id'], $celebrityCat)) && $_product->getSpecialName()) ? $_product->getSpecialName() : $_product->getName();
                $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);
                $subtotal = $_item->getPrice() * $_item->getQty();
                $itemDet[] = array('item_id'=>$_item->getId(), 'entity_id'=>$_item->getProductId(),
                                    'name' => $_product->getName(),
                                    'short_description' => $_product->getShortDescription(),
                                    'quantity'=> $_item->getQty(),
                                    'image_url' => $_product->getImageUrl(),
                                    'regular_price' => $_product->getPrice(),
                                    'final_price' => $_item->getPrice(),
                                    'is_saleable' => $stock->getIsInStock(),
                                    'subtotal' => number_format($subtotal, 4),
                                    );
            }
        }
        return $itemDet;
    }
}

