<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer orderdetails
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Welcome_Mobile_Model_Api2_Customer_Orderdetails_Rest_Guest_V1 extends Welcome_Mobile_Model_Api2_Customer_Orderdetails
{
   protected function _retrieveCollection()
    {
        $post = $this->getRequest()->getParams();
        if ($post) {
        	if($post['lang']){
                $store = Mage::getModel('core/store')->load($post['lang']);
                $storeId = $store->getStoreId();
                $websiteId = $store->getWebsiteId();
                $lang = (strpos($post['lang'], 'ar') == false) ? 1 : 3 ;
            }
            else{
            	$storeId = 19;
                $websiteId = 1;
                $lang = 1;  
            }
        	$customer_id = $post['customer_id'];
        	Mage::app()->setCurrentStore($storeId);
            $decrypt = Mage::getSingleton('core/encryption');
            //$customer_id = base64_decode($decrypt->decrypt($customer_id));
            $customer_id = pack("H*", $customer_id);
            $customer_id = $decrypt->decrypt($customer_id);
		    $customer = Mage::getSingleton('customer/customer')->setWebsiteId($websiteId)->load($customer_id);
		    if($customer->getId()){
                $orderIncrementId = $post['order_id'];
                $response = array();
                $result = array();
		    	try{
		    		$order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
                    if($order->getId() && ($order->getCustomerId()==$customer->getId())){
						$increment_id = $order->getRealOrderId();
						$subTotal = $order->getSubtotal();
						$grandTotal = $order->getGrandTotal();
						$shipping_amount = $order->getShippingAmount();
						$discount = $order->getDiscountAmount();
						$tax = $order->getTaxAmount();
						$codfees = $order->getMspBaseCashondelivery();
						$orderItems = $order->getItemsCollection();
						$billingAddr = $order->getBillingAddress();
						$shippingAddr = $order->getShippingAddress();
						$shipping_method = $order->getShippingDescription();
						$payment = $order->getPayment()->getMethodInstance()->getTitle();
						//get order info
						$response['status'] = 'Success';
	                    //$response['order_id'] = $order->getId();
	                    $response['order_id'] = $increment_id;
	                    $response['order_date'] = $order->getCreatedAt();//Formated('short');
	                    $response['order_status'] = $order->getStatus();
	                    $response['shipping_method'] = $shipping_method;
	                    $response['payment_method'] = $payment;
	                    //get billing and shipping address
                        $response['billing_address'] = array('customer_address_id'=>$billingAddr->getCustomerAddressId(),
                                'firstname' => $billingAddr->getFirstname(),
                                'middlename' => $billingAddr->getMiddlename(),
                                'lastname' => $billingAddr->getLastname(),
                                'company' => $billingAddr->getCompany(),
                                //'addr_block' => $billingAddr->getAddrBlock(),
                                'governorate' => $billingAddr->getGovernorate(),
                                'country_id' => $billingAddr->getCountryId(),
                                //'addr_villa' => $billingAddr->getAddrVilla(),
                                //'addr_avenue' => $billingAddr->getAddrAvenue(),
                                //'addr_street' => $billingAddr->getAddrStreet(),
                                //'addr_flatenumber' => $billingAddr->getAddrFlatenumber(),
                                //'addr_floornumber' => $billingAddr->getAddrFloornumber(),
                                'area' => $billingAddr->getArea() ? $billingAddr->getArea() : '',
                                'street' => $billingAddr->getStreet1() ? $billingAddr->getStreet1() : '',
                                'block' => $billingAddr->getBlock() ? $billingAddr->getBlock() : '',
                                //'address_line_1' => $billingAddr->getStreet1(),
                                'avenue' => $billingAddr->getAvenue(),
                                'telephone'=> $billingAddr->getTelephone(),
                                //'addr_landline' => $billingAddr->getAddrLandline(),
                                'building' => $billingAddr->getBuilding(),
                                'appartment' => $billingAddr->getAppartment(),
                                'tsh_number' => $billingAddr->getTshNumber(),
                                'mobile' => $billingAddr->getMobile()
                        );
                        $response['shipping_address'] = array('customer_address_id'=>$shippingAddr->getCustomerAddressId(),
                            'firstname' => $shippingAddr->getFirstname(),
                            'middlename' => $shippingAddr->getMiddlename(),
                            'lastname' => $shippingAddr->getLastname(),
                            'company' => $shippingAddr->getCompany(),
                            //'addr_block' => $shippingAddr->getAddrBlock(),
                            'governorate' => $shippingAddr->getGovernorate(),
                            'country_id' => $shippingAddr->getCountryId(),
                            //'addr_villa' => $shippingAddr->getAddrVilla(),
                            //'addr_avenue' => $shippingAddr->getAddrAvenue(),
                            //'addr_street' => $shippingAddr->getAddrStreet(),
                            //'addr_flatenumber' => $shippingAddr->getAddrFlatenumber(),
                            //'addr_floornumber' => $shippingAddr->getAddrFloornumber(),
                            'area' => $shippingAddr->getArea() ? $shippingAddr->getArea() : '',
                            'street' => $shippingAddr->getStreet1() ? $shippingAddr->getStreet1() : '',
                            'block' => $shippingAddr->getBlock() ? $shippingAddr->getBlock() : '',
                            //'address_line_1' => $shippingAddr->getStreet1(),
                            'avenue' => $shippingAddr->getAvenue(),
                            'telephone'=> $shippingAddr->getTelephone(),
                            //'addr_landline' => $shippingAddr->getAddrLandline(),
                            'building' => $shippingAddr->getBuilding(),
                            'appartment' => $shippingAddr->getAppartment(),
                            'tsh_number' => $shippingAddr->getTshNumber(),
                            'mobile' => $shippingAddr->getMobile()
                        );
                        
	                    //ordered_items
						foreach ($orderItems as $_item)
                    	{
                        $response['ordered_items'][] = array('item_id'=>$_item->getId(),
                                            'entity_id'=>$_item->getProductId(),
                                            'name' => $_item->getName(),
                                            'quantity'=> $_item->getQtyOrdered(),
                                            'price' => $_item->getPrice(),
                                            'subtotal' => $_item->getRowTotal(),
                                            );    
                    	}
						$total = array();
	                    $total = array('subtotal'=>$subTotal,'shipping'=>$shipping_amount);
	                    if($discount != 0){
	                    	$total[] = $discount;
	                    }
	                    if($tax != 0){
	                    	$total[] = $tax;
	                    }
	                    // if($codfees != 0){
	                    // 	$total['codfees'] = $codfees;	
	                    // }
	                    $total['codfees'] = $codfees;
	                    $total['grandtotal'] = $grandTotal;
	                    foreach ($total as $key => $value) {
							$response['total'][$key] = $value;	                    	
	                    }
	                    $result[] = $response;
	                    return $result;
	                }
	                else{
	                	$msgerror = ($lang == 3) ? Mage::helper('customer')->__('طلب غير صالح أو العميل رقم') : Mage::helper('checkout')->__('Invalid Order or Customer Id');;
				    	$response[] = array('status' => 'error', 'message' => $msgerror);
				    	return $response;
	                }
			    }catch(Exception $e){
			    	$msgerror = $e->getMessage();
			    	$response[] = array('status' => 'error', 'message' => $msgerror);
			    	return $response;
			    }
	        }
	        else{
	        	$msgerror = ($lang == 3) ? Mage::helper('customer')->__('العميل غير صالح') :  Mage::helper('customer')->__('Invalid customer');
		    	$response[] = array('status' => 'error', 'message' => $msgerror);
                return $response;
	        }
    	}
    }
}