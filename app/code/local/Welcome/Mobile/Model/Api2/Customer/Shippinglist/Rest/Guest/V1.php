<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Welcome_Mobile_Model_Api2_Customer_Shippinglist_Rest_Guest_V1 extends Welcome_Mobile_Model_Api2_Customer_Shippinglist
{
    
    protected function _retrieveCollection()
    {
        $post = $this->getRequest()->getParams();
        if ($post) {
            if($post['lang']){
                $store = Mage::getModel('core/store')->load($post['lang']);
                $storeId = $store->getStoreId();
                $websiteId = $store->getWebsiteId();
                $lang = (strchr($post['lang'], 'ar') == false) ? 1 : 3 ;
            }
            else{
                $storeId = 19;
                $websiteId = 1;
                $lang = 1; 
            }
			//echo $websiteId;die;
            Mage::app()->setCurrentStore($storeId);
            $customer_id = $post['customer_id'];
            $decrypt = Mage::getSingleton('core/encryption');
            //$customer_id = base64_decode($decrypt->decrypt($customer_id));
            $customer_id = pack("H*", $customer_id);
            $customer_id = $decrypt->decrypt($customer_id);
            $shipping = array();
            $customer = Mage::getSingleton('customer/customer')->setWebsiteId($websiteId)->load($customer_id);
			//echo $customer->getId();die;
            if($customer->getId()){
                //$customerAddress = Mage::getModel('customer/address');
                $shippingId = $customer->getDefaultShipping();
                $billingId = $customer->getDefaultBilling();
                $primaryShipping = $customer->getDefaultShippingAddress();
                if($websiteId != 1){
                    foreach($customer->getAddresses() as $address){
                        if($address->getDefaultQatarShipping() && $address->getCountryId() == $address->getDefaultShippingCountry() && strchr($post['lang'], strtolower($address->getDefaultShippingCountry()))){
                            $primaryShipping = Mage::getModel("customer/address")->load($address->getId());
                            break;
                        }else{
                            $primaryShipping = array();
                        }
                    }
                }
                $custAddress = array();
                $m = 0;
                //$custAddress[] = $primaryShipping;
				//echo $post['lang']."--".$primaryShipping->getDefaultShippingCountry();die;
				//echo $primaryShipping->getCountryId();die('ok');
                if(isset($primaryShipping['entity_id'])) {
                    if((strchr($post['lang'], strtolower($primaryShipping->getDefaultShippingCountry())) && $primaryShipping->getCountryId() == $primaryShipping->getDefaultShippingCountry()) || ($websiteId == 1 && $primaryShipping->getCountryId() == 'KW')){
                        $custAddress['shipping_address']['shipping_address'][$m]['entity_id'] = $primaryShipping->getId();
                        $custAddress['shipping_address']['shipping_address'][$m]['is_default_shipping'] = 1;
                        $custAddress['shipping_address']['shipping_address'][$m]['firstname'] = $primaryShipping->getFirstname() ? $primaryShipping->getFirstname() : '';
                        $custAddress['shipping_address']['shipping_address'][$m]['middlename'] = $primaryShipping->getMiddlename() ? $primaryShipping->getMiddlename() : '';
                        $custAddress['shipping_address']['shipping_address'][$m]['lastname'] = $primaryShipping->getLastname() ? $primaryShipping->getLastname() : '';
                        $custAddress['shipping_address']['shipping_address'][$m]['company'] = $primaryShipping->getCompany() ? $primaryShipping->getCompany() : '';
                        $custAddress['shipping_address']['shipping_address'][$m]['country_id'] = $primaryShipping->getCountryId() ? $primaryShipping->getCountryId() : '';
                        $custAddress['shipping_address']['shipping_address'][$m]['governorate'] = $primaryShipping->getGovernorate() ? $primaryShipping->getGovernorate() : '';
                        $custAddress['shipping_address']['shipping_address'][$m]['area'] = $primaryShipping->getArea() ? $primaryShipping->getArea() : '';
                        $custAddress['shipping_address']['shipping_address'][$m]['block'] = $primaryShipping->getBlock() ? $primaryShipping->getBlock() : '';
                        $custAddress['shipping_address']['shipping_address'][$m]['street'] = $primaryShipping->getStreet1() ? $primaryShipping->getStreet1() : '';
                        $custAddress['shipping_address']['shipping_address'][$m]['avenue'] = $primaryShipping->getAvenue() ? $primaryShipping->getAvenue() : '';
                        $custAddress['shipping_address']['shipping_address'][$m]['building'] = $primaryShipping->getBuilding() ? $primaryShipping->getBuilding() : '';
                        $custAddress['shipping_address']['shipping_address'][$m]['appartment'] = $primaryShipping->getAppartment() ? $primaryShipping->getAppartment() : '';
                        $custAddress['shipping_address']['shipping_address'][$m]['tsh_number'] = $primaryShipping->getTshNumber() ? $primaryShipping->getTshNumber() : '';
                        //$custAddress['shipping_address']['shipping_address'][$m]['addr_flatenumber'] = $primaryShipping->getAddrFlatenumber() ? $primaryShipping->getAddrFlatenumber() : '';
                        //$custAddress['shipping_address']['shipping_address'][$m]['addr_floornumber'] = $primaryShipping->getAddrFloornumber() ? $primaryShipping->getAddrFloornumber() : '';
                        //$custAddress['shipping_address']['shipping_address'][$m]['street_1'] = $primaryShipping->getStreet1() ? $primaryShipping->getStreet1() : '';
                        //$custAddress['shipping_address']['shipping_address'][$m]['street_2'] = $primaryShipping->getStreet2() ? $primaryShipping->getStreet2() : '';
                        $custAddress['shipping_address']['shipping_address'][$m]['telephone'] = $primaryShipping->getTelephone() ? $primaryShipping->getTelephone() : '';
                        $custAddress['shipping_address']['shipping_address'][$m]['mobile'] = $primaryShipping->getMobile() ? $primaryShipping->getMobile() : '';
                        //$custAddress['shipping_address']['shipping_address'][$m]['addr_landline'] = $primaryShipping->getAddrLandline() ? $primaryShipping->getAddrLandline() : '';
                        //$custAddress['shipping_address']['shipping_address'][$m]['postcode'] = $primaryShipping->getPostcode() ? $primaryShipping->getPostcode() : '';
                        //$custAddress['shipping_address']['shipping_address'][$m]['fax'] = $primaryShipping->getFax() ? $primaryShipping->getFax() : '';
                       // $custAddress['shipping_address']['shipping_address'][$m]['notes'] = $primaryShipping->getNotes() ? $primaryShipping->getNotes() : '';
                        //$custAddress['shipping_address']['shipping_address'][$m]['optional_telephone'] = $primaryShipping->getOptionalTelephone() ? $primaryShipping->getOptionalTelephone() : '';
                        //$custAddress['shipping_address']['shipping_address'][$m]['city'] = $primaryShipping->getCity() ? $primaryShipping->getCity() : '';
                        //$custAddress['shipping_address']['shipping_address'][$m]['addr_block'] = $primaryShipping->getAddrBlock() ? $primaryShipping->getAddrBlock() : '';
                        $m++;
                    }
                }
                if(count($customer->getAdditionalAddresses())){
                    foreach($customer->getAdditionalAddresses() as $address1){
                        //if((strchr($post['lang'], strtolower($address1->getCountryId()))) || ($websiteId == 1 && $address1->getCountryId() == 'KW')){
                            //$custAddress[] = $address1->getData();
                            $custAddress['shipping_address']['shipping_address'][$m]['entity_id'] = $address1->getId();
                            $custAddress['shipping_address']['shipping_address'][$m]['is_default_shipping'] = 0;
                            $custAddress['shipping_address']['shipping_address'][$m]['firstname'] = $address1->getFirstname() ? $address1->getFirstname() : '';
                            $custAddress['shipping_address']['shipping_address'][$m]['middlename'] = $address1->getMiddlename() ? $address1->getMiddlename() : '';
                            $custAddress['shipping_address']['shipping_address'][$m]['lastname'] = $address1->getLastname() ? $address1->getLastname() : '';
                            $custAddress['shipping_address']['shipping_address'][$m]['company'] = $address1->getCompany() ? $address1->getCompany() : '';
                            $custAddress['shipping_address']['shipping_address'][$m]['country_id'] = $address1->getCountryId() ? $address1->getCountryId() : '';
                            $custAddress['shipping_address']['shipping_address'][$m]['governorate'] = $address1->getGovernorate() ? $address1->getGovernorate() : '';
                            $custAddress['shipping_address']['shipping_address'][$m]['area'] = $address1->getArea() ? $address1->getArea() : '';
                            $custAddress['shipping_address']['shipping_address'][$m]['block'] = $address1->getBlock() ? $address1->getBlock() : '';
                            $custAddress['shipping_address']['shipping_address'][$m]['street'] = $address1->getStreet1() ? $address1->getStreet1() : '';
                            $custAddress['shipping_address']['shipping_address'][$m]['avenue'] = $address1->getAvenue() ? $address1->getAvenue() : '';
                            $custAddress['shipping_address']['shipping_address'][$m]['building'] = $address1->getBuilding() ? $address1->getBuilding() : '';
                            $custAddress['shipping_address']['shipping_address'][$m]['appartment'] = $address1->getAppartment() ? $address1->getAppartment() : '';
                            $custAddress['shipping_address']['shipping_address'][$m]['tsh_number'] = $address1->getTshNumber() ? $address1->getTshNumber() : '';
                            //$custAddress['shipping_address']['shipping_address'][$m]['street_1'] = $address1->getStreet1() ? $address1->getStreet1() : '';
                            //$custAddress['shipping_address']['shipping_address'][$m]['street_2'] = $address1->getStreet2() ? $address1->getStreet2() : '';
                            $custAddress['shipping_address']['shipping_address'][$m]['telephone'] = $address1->getTelephone() ? $address1->getTelephone() : '';
                            $custAddress['shipping_address']['shipping_address'][$m]['mobile'] = $address1->getMobile() ? $address1->getMobile() : '';
                            //$custAddress['shipping_address']['shipping_address'][$m]['addr_landline'] = $address1->getAddrLandline() ? $address1->getAddrLandline() : '';
                            //$custAddress['shipping_address']['shipping_address'][$m]['postcode'] = $address1->getPostcode() ? $address1->getPostcode() : '';
                            //$custAddress['shipping_address']['shipping_address'][$m]['fax'] = $address1->getFax() ? $address1->getFax() : '';
                            //$custAddress['shipping_address']['shipping_address'][$m]['notes'] = $address1->getNotes() ? $address1->getNotes() : '';
                            //$custAddress['shipping_address']['shipping_address'][$m]['optional_telephone'] = $address1->getOptionalTelephone() ? $address1->getOptionalTelephone() : '';
                            //$custAddress['shipping_address']['shipping_address'][$m]['city'] = $address1->getCity() ? $address1->getCity() : '';
                            //$custAddress['shipping_address']['shipping_address'][$m]['addr_block'] = $address1->getAddrBlock() ? $address1->getAddrBlock() : '';
                            $m++;
                        //}
                    }   
                }
                //get shipping methods
                $shipmethods = Mage::getSingleton('shipping/config')->getActiveCarriers($storeId);
                $shipping_method = array();
                foreach($shipmethods as $carrierCode => $carrierModel){ 
                    $ship1 = array();                           
                   if( $carrierMethods = $carrierModel->getAllowedMethods() )
                   {
                       foreach ($carrierMethods as $methodCode => $method)
                       {    
                            $code = $carrierCode.'_'.$methodCode;
                       }
                       $carrierTitle = Mage::getStoreConfig('carriers/'.$carrierCode.'/title', $storeId);
                       $price = Mage::getStoreConfig('carriers/'.$carrierCode.'/price', $storeId);
                       $ship1['code'] = $code;
                       $ship1['title'] = $carrierTitle;
                       $ship1['shipping_price'] = $price;
                       $shipp['shipping_method'][] = $ship1;
                   }
                }
                if($m>0){
                    $custAddress['shipping_method'] = $shipp;   
                }
				Mage::log('Request ----',null,'shippinglistapi.log');
				Mage::log($post,null,'shippinglistapi.log');
				Mage::log('Response ----',null,'shippinglistapi.log');
				Mage::log($custAddress,null,'shippinglistapi.log');
                return $custAddress;
            }
            else{
                $error = array();
                $message = ($lang == 3) ? Mage::helper('customer')->__('دخول العملاء صحيح') : Mage::helper('customer')->__("Enter valid customer");
                $error['status'] = $customer_id;
                $error['message'] = $message;
                $shipping[] = $error;
                return $shipping;   
            }
        }
    }
}