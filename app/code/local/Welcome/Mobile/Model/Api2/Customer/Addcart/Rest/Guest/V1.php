<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Welcome_Mobile_Model_Api2_Customer_Addcart_Rest_Guest_V1 extends Welcome_Mobile_Model_Api2_Customer_Addcart
{
   protected function _create()
    {
        $post = $this->getRequest()->getBodyParams();
        $inclue = 0;

        if ($post) {
            $customer_id = $post['customer_id'];
            //$storeId = ($post['lang'] == 'ar') ? 3 : 1;
            if($post['lang']){
                $store = Mage::getModel('core/store')->load($post['lang']);
                $storeId = $store->getStoreId();
                $websiteId = $store->getWebsiteId();
                $lang = (strchr($post['lang'], 'arabic') === false) ? 1 : 3 ;
            }
            else{
                $websiteId = 19;
                $storeId = 1;
                $lang = 1;
            }
            $website = Mage::getModel('core/website')->load($websiteId);
            foreach ($website->getGroups() as $group) {
                $stores = $group->getStores();
                foreach ($stores as $store) {
                    $store_id[] = $store->getId();
                }
            }
            Mage::app()->setCurrentStore($storeId);
            $decrypt = Mage::getSingleton('core/encryption');
            $customer_id = pack("H*", $customer_id);
            $customer_id = $decrypt->decrypt($customer_id);
            $customer = Mage::getSingleton('customer/customer')->setWebsiteId($websiteId)->load($customer_id);
            if($customer->getId()){
                if(isset($post['product_id'])){
                    $product_id = $post['product_id'];
                }
                if(isset($post['quantity'])){
                    $qty = $post['quantity'];
                }
                if(isset($post['category'])){
                    $cat = $post['category'];
                }                
                $quantity = array();
                $category = array();
                $productIds = array();
                $productIds = explode(',', $product_id);
                $quantity = explode(',', $qty);
                $category = explode(',', $cat);
                try{
                    $newqty = array();
                    for($i = 0; $i < count($productIds); $i++){
                       $nqty = isset($quantity[$i]) ? $quantity[$i] : 1;
                       $ncat = isset($category[$i]) ? $category[$i] : -1;
                       $npro = isset($productIds[$i]) ? $productIds[$i] : 0;
                       $newqty[$i] = $nqty.'_'.$ncat.'_'.$npro;
                    }
                    //$items = array_combine($productIds,$newqty);
                    $dismessage = '';

                    $quote = Mage::getModel('sales/quote')->getCollection()->addFieldToFilter('store_id', array('in', $store_id))->addFieldToFilter('customer_id', $customer->getId())->addFieldToFilter('is_active', 1)->setOrder('entity_id', 'desc')->getFirstItem();
                    if(!$quote->getId()){
                        $quote = Mage::getModel('sales/quote');
                        $quote->setCustomerId($customer->getId());
                    }

                    $quote->assignCustomer($customer);
                    $quote->setStoreId($storeId);
                    //$celebrity = Mage::helper('celebrity')->getCelebrityCategoryIds();
                    Mage::register('store_id',(int)$storeId);
                    Mage::register('quote_id',(int)$quote->getId());
                    foreach($newqty as $key=>$value){
                        $qtycat = explode('_', $value);
                        /*if($qtycat[1] == '' || $qtycat[1]<=0 || (!in_array($qtycat[1], $celebrity))){
                            $qtycat[1] = -1;
                        })*/
                        if($qtycat[0] == ''){
                            $qtycat[0] = 1;
                        }
                        if($qtycat[2] == ''){
                            $qtycat[2] = 0;
                        }
                        $product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($qtycat[2]);
                        //get final price from celebrity
                        /*$data = unserialize($product->getCelebritySpecialPrice());
                        $cel_price = (isset($data[$qtycat[1]])) ? $data[$qtycat[1]] : $product->getFinalPrice();*/
                        if( $product->getFinalPrice() <= 0 ){
                            if(count($newqty)==1){
                                if($lang == 3){
                                    $err = (!$product->getId()) ? Mage::helper('customer')->__('معرف المنتج غير صالح') : Mage::helper('customer')->__('لا يمكنك إضافة المنتج مع سعر الصفر');
                                }
                                else{
                                    $err = (!$product->getId()) ? Mage::helper('customer')->__('Invalid Product ID') : Mage::helper('customer')->__('Cannot add product with zero price');
                                }
                                throw new Exception($err);
                            }
                            continue;
                        }
                        if($product->getStatus()==2){
                            if(count($newqty)==1){
                                $err = ($lang == 3) ? Mage::helper('customer')->__('تم تعطيل %s المنتج', $product->getName()) : Mage::helper('customer')->__('The product %s is disabled', $product->getName());
                                
                                throw new Exception($err);
                            }
                            else{
                                $dismessage .=',';
                                $dismessage .= ($lang == 3) ? Mage::helper('customer')->__('تم تعطيل %s المنتج', $product->getName()) : Mage::helper('customer')->__('The product %s is disabled', $product->getName());
                                $dismessage .='';
                                continue;
                            }
                        }
						
						//$finalPrice=$product->getFinalPrice();
						//$product->setFinalPrice($price);
						//echo $finalPrice;die;
                        //$product = Mage::getModel('catalog/product')->setStoreId(1)->load(2012);
                        $params = array(
                            'product' => (int)$qtycat[2],
                            'qty' => (int)$qtycat[0],
                            'category'=> (int)$qtycat[1],
                            'request_type'=>'restapi',
							//'price'=>0
                        );
												
                        //Qty restriction by celebrities
                        /*$item_qty = $quote->getItemQtyForRestrict((int)$qtycat[2] , (int)$qtycat[1]);
                        $getAllowedQty = $quote->getAllowedQty((int)$qtycat[2] , (int)$qtycat[1]);
                        $paramsQty = (int)$qtycat[0];
                        $fututureQty = (int)$paramsQty + $item_qty;
                        if((int)$qtycat[1] && $fututureQty>6 && $getAllowedQty>6){
                        	$inclue = 1;
                            if($lang == 1){
                                Mage::throwException('Maximum number of qty per item is 6. Already '.$item_qty.' qty(s) in your bag.');
                            }
                            else {
                                Mage::throwException('الحد الأقصى لعدد الكمية لكل بند هو 6. إذا كنت '.$item_qty.' الكمية (ق) في حقيبتك.');
                            }
                        }elseif((int)$qtycat[1] && $fututureQty>$getAllowedQty){
                        	$inclue = 1;
                            if($lang == 1){
                                Mage::throwException('The Item you have selected is SOLD OUT');
                            }
                            else {
                                Mage::throwException('المنتج غير متوفر بالمخزون');
                            }
                        }*/
                        //Mage::register('cat_id',(int)$qtycat[1]);
                        $request = new Varien_Object();
                        $request->setData($params);

                        $result = $quote->addProduct($product, $request);
						
                        //Mage::unregister('cat_id');
                    }
					
                    $quote->collectTotals();
					$quoteCount = $quote->getItemsCount();
					
                    /*$quoteCount = $quote->getItemsCount();
                    //check line item count
                    if($quoteCount > 15) {
                    	$inclue = 1;
                        if($lang == 1){
                            Mage::throwException('Maximum number of items per order is 15');
                        }
                        else {
                            Mage::throwException('الحد الأقصى لعدد العناصر في النظام هو 15');
                        }
                    }*/
                   /* $shipmethods = Mage::getSingleton('shipping/config')->getActiveCarriers($storeId);
                    $shipping_method = array();
                    foreach($shipmethods as $carrierCode => $carrierModel){ 
                        $shipping = array();                           
                       if( $carrierMethods = $carrierModel->getAllowedMethods() )
                       {
                           foreach ($carrierMethods as $methodCode => $method)
                           {    
                                $code = $carrierCode.'_'.$methodCode;
                           }
                           $ship_price = Mage::getStoreConfig('carriers/'.$carrierCode.'/price', $storeId);
                       }
                    }
                    if($websiteId == 2){
                        $quote_price = $ship_price + $quote->getBaseGrandTotal();
                        $quote_price = Mage::helper('directory')->currencyConvert($quote_price, 'QAR', 'KWD');
                    }
                    else{
                        $quote_price = $quote->getBaseGrandTotal() + $ship_price;
                    }
                    //restrict max order amount that specified in admin
                    $max_amount_enable = Mage::getStoreConfig('sales/inchoo_maxorderamount/active');
                    $max_amount = Mage::getStoreConfig('sales/inchoo_maxorderamount/single_order_top_amount');
                    $max_amount_message = Mage::getStoreConfig('sales/inchoo_maxorderamount/single_order_top_amount_msg');
                    if($max_amount_enable && $quote_price > $max_amount){
                    	$inclue = 1;
                        if($websiteId == 1){
                            if($lang == 1){
                                throw new Exception(Mage::helper('customer')->__('No single order allowed with amount over '.$max_amount.' KD'));
                            }
                            else {
                                throw new Exception(Mage::helper('customer')->__('أي أمر واحد يسمح مع مبلغ أكثر '.$max_amount.' KD'));
                            }
                        }else{
                            if($lang == 1){
                                throw new Exception(Mage::helper('customer')->__('No single order allowed with amount over 4800 QR'));
                            }
                            else {
                                throw new Exception(Mage::helper('customer')->__('أي أمر واحد يسمح مع مبلغ أكثر 4800 QR'));
                            }
                        }
                    }*/
					//$quote_price = $quote->getBaseGrandTotal();
                    $quote->save();
					
                    Mage::unregister('quote_id');
                    Mage::unregister('store_id');
                    $msgsuccess = ($lang == 3) ? Mage::helper('customer')->__('تمت إضافة المنتج إلى سلة التسوق') : Mage::helper('customer')->__('The product has been added to cart');
                    $msgsuccess .= $dismessage;
                    $itemDet = $this->getAvailableCartItems($quote->getId(), $storeId, $customer->getId(), $store_id);
                    $info = array('status' => 'Success', 'message' => $msgsuccess);
					Mage::log('Response ----',null,'addtocart.log');
					Mage::log($info,null,'addtocart.log');
					Mage::log($itemDet,null,'addtocart.log');
                    echo json_encode(array_merge($info, array('items'=>$itemDet)));
                    exit();
                }catch(Exception $e){
                    $msgerror = $e->getMessage().''.$dismessage;
                    $info = array('status' => 'error', 'message' => $msgerror);
                    $itemDet = array();
                    if($inclue){
                        $itemDet =$this->getAvailableCartItems($quote->getId(), $storeId, $customer->getId(), $store_id);
                    }
					Mage::log('Response ----',null,'addtocart.log');
					Mage::log($info,null,'addtocart.log');
					Mage::log($itemDet,null,'addtocart.log');
                    echo json_encode(array_merge($info,array('items'=>$itemDet)));
                    exit();
                }
            }
            else{
                $msgerror = ($lang == 3) ? Mage::helper('customer')->__('العميل غير صالح') : Mage::helper('customer')->__('Invalid customer');
                $json = array('status' => 'error', 'message' => $msgerror);
				Mage::log('Response ----',null,'addtocart.log');
				Mage::log($json,null,'addtocart.log');
                echo json_encode($json);
                exit();
            }
        }
    }
    public function getAvailableCartItems($quote_id, $storeId, $customer_id, $store_id){
        $itemDet = array();
        $equote = Mage::getModel('sales/quote')->getCollection()->addFieldToFilter('store_id', array('in', $store_id))->addFieldToFilter('customer_id', $customer_id)->addFieldToFilter('is_active', 1)->setOrder('entity_id', 'desc')->getFirstItem();
        if($equote->getId()){
           $quoteCollection = $equote->getAllVisibleItems();
           //get celebrities children
            //$category = Mage::getModel('catalog/category')->load(95);
            //$celebrityCat = explode(',', $category->getChildren());
            //$categoryIds = Mage::helper('celebrity')->getCelebrityCategoryIds();
			//count($quoteCollection);die;
            foreach ($quoteCollection as $_item)
            {
                $_product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($_item->getProductId());
                //$splname = ((in_array($_item['category_id'], $celebrity) || in_array($_item['category_id'], $celebrityCat)) && $_product->getSpecialName()) ? $_product->getSpecialName() : $_product->getName();
                $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);
                $subtotal = $_item->getPrice() * $_item->getQty();
                $itemDet[] = array('item_id'=>$_item->getId(),
                                    'entity_id'=>$_item->getProductId(),
                                    'name' => $_product->getName(),
                                    'short_description'=>$_product->getShortDescription(),
                                    'quantity'=> $_item->getQty(),
                                    'image_url' => $_product->getImageUrl(),
                                    'regular_price' => $_product->getPrice(),
                                    'final_price' => $_item->getPrice(),
                                    'is_saleable'=> $stock->getIsInStock(),
                                    'subtotal' => number_format($subtotal, 4),
                                    );    
            }
        }
        return $itemDet;
    }
}
