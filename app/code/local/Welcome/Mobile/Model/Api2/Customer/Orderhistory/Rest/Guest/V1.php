<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Welcome_Mobile_Model_Api2_Customer_Orderhistory_Rest_Guest_V1 extends Welcome_Mobile_Model_Api2_Customer_Orderhistory
{
   protected function _retrieveCollection()
    {
        $post = $this->getRequest()->getParams();
        if ($post) {
        	if($post['lang']){
                $store = Mage::getModel('core/store')->load($post['lang']);
                $storeId = $store->getStoreId();
                $websiteId = $store->getWebsiteId();
                $lang = (strpos($post['lang'], 'ar') == false) ? 1 : 3 ;
            }
            else{
                $websiteId = 19;  
                $lang = 1;
            }
            $website = Mage::getModel('core/website')->load($websiteId);
            foreach ($website->getGroups() as $group) {
                $stores = $group->getStores();
                foreach ($stores as $store) {
                    $store_id[] = $store->getId();
                }
            }
        	$customer_id = $post['customer_id'];
        	//Mage::app()->setCurrentStore($storeId);
            $decrypt = Mage::getSingleton('core/encryption');
            //$customer_id = base64_decode($decrypt->decrypt($customer_id));
            $customer_id = pack("H*", $customer_id);
            $customer_id = $decrypt->decrypt($customer_id);
		    $customer = Mage::getSingleton('customer/customer')->load($customer_id);
		    if($customer->getId()){
                $orders = Mage::getResourceModel('sales/order_collection')
				            ->addFieldToSelect('*')
				            ->addFieldToFilter('store_id', array('in', $store_id))
				            ->addFieldToFilter('customer_id', $customer->getId())
				            ->addFieldToFilter('state', array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates()))
				            ->setOrder('created_at', 'desc');
		    	try{
		    		if(!count($orders)){
						throw new Exception("You have placed no orders.", 1);
					}
		    		$orderDet = array();
		    		foreach($orders as $_order){
			    		$order = Mage::getModel("sales/order")->load($_order->getId());
	                    if($order->getId() && ($order->getCustomerId()==$customer->getId())){
							$increment_id = $order->getRealOrderId();
							$grandTotal = $order->getGrandTotal();
							$shippingAddr = $order->getShippingAddress();
							$ordered_date = Mage::getModel('core/date')->date('m/d/Y', strtotime($order->getCreatedAt()));
                            $shipping = array('shipping_address'=>array('customer_address_id'=>$shippingAddr->getCustomerAddressId(),
                                    'firstname' => $shippingAddr->getFirstname(),
                                    'middlename' => $shippingAddr->getMiddlename(),
                                    'lastname' => $shippingAddr->getLastname(),
                                    'company' => $shippingAddr->getCompany(),
                                    'country_id' => $shippingAddr->getCountryId(),
                                    'governorate' => $shippingAddr->getGovernorate(),
				                    'area' => $shippingAddr->getArea(),
				                    'block' => $shippingAddr->getBlock(),
				                    'street' => $shippingAddr->getStreet1(),
				                    'avenue' => $shippingAddr->getAvenue(),
				                    'building' => $shippingAddr->getBuilding(),
				                    'appartment' => $shippingAddr->getAppartment(),
				                    'tsh_number' => $shippingAddr->getTshNumber(),
                                    //'city' => $shippingAddr->getCity(),
                                    //'addr_block' => $shippingAddr->getAddrBlock(),
                                    //'addr_villa' => $shippingAddr->getAddrVilla(),
                                    //'addr_avenue' => $shippingAddr->getAddrAvenue(),
                                    //'addr_street' => $shippingAddr->getAddrStreet(),
                                    //'addr_flatenumber' => $shippingAddr->getAddrFlatenumber(),
                                    //'addr_floornumber' => $shippingAddr->getAddrFloornumber(),
                                    //'state' => $shippingAddr->getRegion() ? $shippingAddr->getRegion() : '',
                                    //'street_1' => $shippingAddr->getStreet1() ? $shippingAddr->getStreet1() : '',
                                    //'street_2' => $shippingAddr->getStreet2() ? $shippingAddr->getStreet2() : '',
                                    //'address_line_1' => $shippingAddr->getStreet1(),
                                    //'postcode' => $shippingAddr->getPostcode(),
                                    'telephone'=> $shippingAddr->getTelephone(),
                                    'mobile'=> $shippingAddr->getMobile(),
                                    //'addr_landline' => $shippingAddr->getAddrLandline(),
                                    //'fax' => $shippingAddr->getFax(),
                                    //'first_id_proof' => $shippingAddr->getFirstIdProof() ? Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).$shippingAddr->getFirstIdProof() : '',
                                    //'second_id_proof' => $shippingAddr->getSecondIdProof() ? Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).$shippingAddr->getSecondIdProof() : ''
                                )
                            );
		                    $info = array('order_id'=>$increment_id, 
		                    			//'order_date'=>$order->getCreatedAtFormated('short'),
		                    			'order_date'=>$order->getCreatedAt(),
		                    			'order_status'=>$order->getStatus(),
		                    			'grandtotal' => $grandTotal);
		                    $orderDet['orders'][] = array_merge($info, $shipping);
		                    //$response['order_history'][] = array_merge($info, $shipping);
		                }
		                else{
		                	$msgerror = ($lang == 3) ? Mage::helper('customer')->__('طلب غير صالح أو العميل رقم') : Mage::helper('checkout')->__('Invalid Order or Customer Id');
					    	$response[] = array('status' => 'error', 'message' => $msgerror);
					    	return $response;
		                }
		            }
		            $response['status'] = 'Success';
		            $response['order_history'] = $orderDet;
		            return $response;
			    }catch(Exception $e){
			    	$msgerror = $e->getMessage();
			    	$response[] = array('status' => 'error', 'message' => $msgerror);
			    	return $response;
			    }
	        }
	        else{
	        	$msgerror = Mage::helper('customer')->__('Invalid customer');
		    	$response[] = array('status' => 'error', 'message' => $msgerror);
		    	return $response;
	        }
    	}
    }
}